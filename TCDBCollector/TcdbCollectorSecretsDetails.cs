﻿

namespace EonData.TornCity.Tcdb.Collector {
    internal class TcdbCollectorSecretsDetails {
        public string ApiKeySecret { get; set; }
        public string DbAuthSecret { get; set; }
        public string DbConnSecret { get; set; }
    }
}
