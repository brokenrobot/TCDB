﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using EonData.TornCity.Api;
using System.Diagnostics;

namespace EonData.TornCity.Tcdb.Collector {
    /// <summary>
    /// Manages actions with the TCDB database.
    /// </summary>
    internal sealed class TcdbCollectorDatabase : IAsyncDisposable {
        public const string CollectorAppName = "Torn Data Collector]";
        public const string LandingSchema = "landing";
        public const string DataVaultSchema = "dv";
        public const string StagingSchema = "stage";
        public const string LoggingSchema = "memoir";

        public long CollectorLogKey { get; private set; }

        private readonly SqlConnection sqldb;

        /// <summary>
        /// Creates a new TCDB database object.
        /// </summary>
        /// <param name="dbConnectionString">Connection string ot use for the database.</param>
        internal TcdbCollectorDatabase(string dbConnStr) {
            var connStr = new SqlConnectionStringBuilder(dbConnStr) {
                ApplicationName = CollectorAppName
            };
            sqldb = new SqlConnection(connStr.ConnectionString);
        }

        public async Task OpenAsync(CancellationToken cancellationToken) {
            // open the sql database connection
            await sqldb.OpenAsync(cancellationToken);

            // get some info about the collector host instance
            var collectorInst = new TcdbCollectorInstanceDetails();

            // log this information and retrieve the log key for this collector
            await using SqlCommand cmd = CreateCommand($"[{LoggingSchema}].[Log_Collector_Startup]", CommandType.StoredProcedure);
            cmd.Parameters.Add(CreateParameter("@Hostname", SqlDbType.NVarChar, 2000, collectorInst.Hostname));
            cmd.Parameters.Add(CreateParameter("@CollectorVersion", SqlDbType.NVarChar, 50, collectorInst.AppVersion));
            cmd.Parameters.Add(CreateParameter("@DotNetVersion", SqlDbType.NVarChar, 50, collectorInst.DotNetVersion));
            cmd.Parameters.Add(CreateParameter("@OperatingSystem", SqlDbType.NVarChar, 500, collectorInst.OperatingSystem));
            cmd.Parameters.Add(CreateOutputParameter("@CollectorLogKey", SqlDbType.BigInt));
            await cmd.ExecuteNonQueryAsync(cancellationToken);

            if (cmd.Parameters["@CollectorLogKey"].Value == null) {
                CollectorLogKey = -1;
            }
            else {
                CollectorLogKey = (long)cmd.Parameters["@CollectorLogKey"].Value;
            }
        }

        /// <summary>
        /// Imports data and then proceses it into the warehouse.
        /// </summary>
        /// <param name="element">Name of the element the data is for.</param>
        /// <param name="data">A <see cref="DataTable"/> containing the data to import.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task ProcessData(string element, DataTable data, CancellationToken cancellationToken) {
            await TruncateTableAsync(LandingSchema, element, cancellationToken);

            // bulk copy the data retrieved from the api into the landing table
            using (var bc = new SqlBulkCopy(sqldb) {
                DestinationTableName = $"[{LandingSchema}].[{element}]",
                BulkCopyTimeout = 300
            }) {
                await bc.WriteToServerAsync(data, cancellationToken);
            }

            // log the number of rows exported from the api
            await using (SqlCommand logRowCountCmd = CreateCommand($"[{LoggingSchema}].[Log_Landing_Count]", CommandType.StoredProcedure)) {
                logRowCountCmd.Parameters.Add(CreateParameter("@TableName", SqlDbType.NVarChar, 100, element));
                logRowCountCmd.Parameters.Add(CreateParameter("@RowsExported", SqlDbType.Int, data.Rows.Count));
                logRowCountCmd.Parameters.Add(CreateParameter("@CollectorLogKey", SqlDbType.BigInt, CollectorLogKey));
                await logRowCountCmd.ExecuteNonQueryAsync(cancellationToken);
            }


            // call the stored procedure that processes the landed data into the data warehouse.
            await using (SqlCommand procDataCmd = CreateCommand($"[dbo].[Process_{element}_data]", CommandType.StoredProcedure)) {
                procDataCmd.Parameters.Add(CreateParameter("@CollectorLogKey", SqlDbType.BigInt, CollectorLogKey));
                await procDataCmd.ExecuteNonQueryAsync(cancellationToken);
            }
        }

        public ValueTask DisposeAsync() => sqldb.DisposeAsync();

        private SqlParameter CreateOutputParameter(string name, SqlDbType dataType) => new SqlParameter(name, dataType) { Direction = ParameterDirection.Output };

        // integers and other types that don't have a length/etc
        private SqlParameter CreateParameter(string name, SqlDbType dataType, object value) => new SqlParameter(name, dataType) { Value = GetValue(value) };

        // strings and other types with a length
        private SqlParameter CreateParameter(string name, SqlDbType dataType, int length, object value) => new SqlParameter(name, dataType, length) { Value = GetValue(value) };

        // decimals and other types with a precision and scale
        private SqlParameter CreateParameter(string name, SqlDbType datatype, byte precision, byte scale, object value) => new SqlParameter(name, datatype) { Precision = precision, Scale = scale, Value = GetValue(value) };

        private object GetValue(object value) => value ?? DBNull.Value;

        /// <summary>
        /// Truncates a table.
        /// </summary>
        /// <param name="schema">Name of the schema the table is in.</param>
        /// <param name="table">Name of the table.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        private async Task TruncateTableAsync(string schema, string table, CancellationToken cancellationToken) {
            await using SqlCommand cmd = CreateCommand($"TRUNCATE TABLE [{schema}].[{table}];", CommandType.Text);
            await cmd.ExecuteNonQueryAsync(cancellationToken);
        }

        /// <summary>
        /// Creates a <see cref="SqlCommand"/> with the standard configuration.
        /// </summary>
        /// <param name="query">SQL query for the command.</param>
        /// <param name="ctype">Type of command.</param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "Used by the methods in this class to create the SQL statements used to load data. It is supposed to take dynamic queries.")]
        private SqlCommand CreateCommand(string query, CommandType ctype) {
            SqlCommand c = sqldb.CreateCommand();
            c.CommandType = ctype;
            c.CommandText = query;
            c.CommandTimeout = 300;
            return c;
        }
    }
}
