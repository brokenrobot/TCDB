﻿using EonData.TornCity.Api;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EonData.TornCity.Tcdb.Collector {
    /// <summary>
    /// Manages exporting data from the Torn API
    /// </summary>
    internal class TcdbCollectorApi : IDisposable {
        private readonly TornApi tapi;

        /// <summary>
        /// Creates a new TcdBCollectorApi object.
        /// </summary>
        /// <param name="apiKey">API key to use when exporting data.</param>
        public TcdbCollectorApi(string apiKey) => tapi = new TornApi(apiKey);

        /// <summary>
        /// Gets Torn item data.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>A <see cref="=DataTable"/> containing the Torn item data.</returns>
        public async Task<DataTable> GetItemDataAsync(CancellationToken cancellationToken) {
            // create table with the appropriate schema
            DataTable dtable = CreateTableSchema(new Dictionary<string, Type>() {
                { "ItemId", typeof(int) },
                { "ItemName", typeof(string) },
                { "ItemDescription", typeof(string) },
                { "ItemType", typeof(string) },
                { "WeaponType", typeof(string) },
                { "BuyPrice", typeof(long) },
                { "SellPrice", typeof(long) },
                { "MarketValue", typeof(long) },
                { "Circulation", typeof(long) },
                { "Effect", typeof(string) },
                { "Requirement", typeof(string) },
                { "ImageUrl", typeof(string) },
            });

            // read data from the api into the DataTable
            Dictionary<int, TornItem> data = await tapi.GetItemsAsync(cancellationToken);
            foreach ((int id, TornItem item) in data) {
                DataRow newRow = dtable.NewRow();
                newRow["ItemId"] = ProcessValue(id);
                newRow["ItemName"] = ProcessValue(item.Name);
                newRow["ItemDescription"] = ProcessValue(item.Description);
                newRow["ItemType"] = ProcessValue(item.Category);
                newRow["WeaponType"] = ProcessValue(item.WeaponType);
                newRow["BuyPrice"] = ProcessValue(item.BuyPrice);
                newRow["SellPrice"] = ProcessValue(item.SellPrice);
                newRow["MarketValue"] = ProcessValue(item.MarketValue);
                newRow["Circulation"] = ProcessValue(item.Circulation);
                newRow["Effect"] = ProcessValue(item.Effect);
                newRow["Requirement"] = ProcessValue(item.Requirement);
                newRow["ImageUrl"] = ProcessValue(item.ImageUrl);
                dtable.Rows.Add(newRow);
            }

            return dtable;
        }

        /// <summary>
        /// Gets Torn stock market data.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>A <see cref="=DataTable"/> containing the Torn stock market data.</returns>
        public async Task<DataTable> GetStockDataAsync(CancellationToken cancellationToken) {
            DataTable dtable = CreateTableSchema(new Dictionary<string, Type>() {
                { "StockId", typeof(int) },
                { "StockName", typeof(string) },
                { "StockAcronym", typeof(string) },
                { "DirectorName", typeof(string) },
                { "CurrentPrice", typeof(decimal) },
                { "MarketCap", typeof(long) },
                { "TotalShares", typeof(long) },
                { "Forecast", typeof(string) },
                { "Demand", typeof(string) },
                { "BenefitRequirement", typeof(long) },
                { "BenefitDescription", typeof(string) },
            });

            Dictionary<int, TornStock> data = await tapi.GetStocksAsync(cancellationToken);
            foreach((int id, TornStock stock) in data) {
                DataRow newRow = dtable.NewRow();
                newRow["StockId"] = ProcessValue(id);
                newRow["StockName"] = ProcessValue(stock.Name);
                newRow["StockAcronym"] = ProcessValue(stock.Acronym);
                newRow["DirectorName"] = ProcessValue(stock.Director);
                newRow["CurrentPrice"] = ProcessValue(stock.CurrentPrice);
                newRow["MarketCap"] = ProcessValue(stock.MarketCap);
                newRow["TotalShares"] = ProcessValue(stock.TotalShares);
                newRow["Forecast"] = ProcessValue(stock.Forecast);
                newRow["Demand"] = ProcessValue(stock.Demand);
                newRow["BenefitRequirement"] = ProcessValue(stock.Benefit?.Requirement);
                newRow["BenefitDescription"] = ProcessValue(stock.Benefit?.Description);
                dtable.Rows.Add(newRow);
            }

            return dtable;
        }

        /// <summary>
        /// Gets Torn gym data.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>A <see cref="=DataTable"/> containing the Torn gym data.</returns>
        public async Task<DataTable> GetGymDataAsync(CancellationToken cancellationToken) {
            DataTable dtable = CreateTableSchema(new Dictionary<string, Type>() {
                { "GymId", typeof(int) },
                { "GymName", typeof(string) },
                { "Stage", typeof(int) },
                { "Cost", typeof(int) },
                { "Energy", typeof(decimal) },
                { "Strength", typeof(decimal) },
                { "Speed", typeof(decimal) },
                { "Defense", typeof(decimal) },
                { "Dexterity", typeof(decimal) },
                { "Note", typeof(string) },
            });

            Dictionary<int, TornGym> data = await tapi.GetGymsAsync(cancellationToken);
            foreach ((int id, TornGym gym) in data) {
                DataRow newRow = dtable.NewRow();
                newRow["GymId"] = ProcessValue(id);
                newRow["GymName"] = ProcessValue(gym.Name);
                newRow["Stage"] = ProcessValue(gym.Stage);
                newRow["Cost"] = ProcessValue(gym.Cost);
                newRow["Energy"] = ProcessValue(gym.Energy);
                newRow["Strength"] = ProcessValue(gym.Strength);
                newRow["Speed"] = ProcessValue(gym.Speed);
                newRow["Defense"] = ProcessValue(gym.Defense);
                newRow["Dexterity"] = ProcessValue(gym.Dexterity);
                newRow["Note"] = ProcessValue(gym.Note);
                dtable.Rows.Add(newRow);
            }

            return dtable;
        }

        /// <summary>
        /// Gets Torn game stat data.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>A <see cref="=DataTable"/> containing the Torn game stat data.</returns>
        public async Task<DataTable> GetGameStatDataAsync(CancellationToken cancellationToken) {
            DataTable dtable = CreateTableSchema(new Dictionary<string, Type>() {
                { "StatName", typeof(string) },
                { "StatValue", typeof(decimal) }
            });

            IDictionary<string, decimal> data = await tapi.GetGameStatsAsync(cancellationToken);
            foreach ((string name, decimal value) in data) {
                DataRow newRow = dtable.NewRow();
                newRow["StatName"] = ProcessValue(name);
                newRow["StatValue"] = ProcessValue(value);
                dtable.Rows.Add(newRow);
            }

            return dtable;
        }

        /// <summary>
        /// Gets Torn honor bar data.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>A <see cref="=DataTable"/> containing the Torn honor bar data.</returns>
        public async Task<DataTable> GetHonorDataAsync(CancellationToken cancellationToken) {
            DataTable dtable = CreateTableSchema(new Dictionary<string, Type>() {
                { "HonorId", typeof(int) },
                { "HonorName", typeof(string) },
                { "HonorDescription", typeof(string) },
                { "HonorType", typeof(int) },
                { "Circulation", typeof(long) },
                { "Rarity", typeof(string) }
            });

            Dictionary<int, TornHonor> data = await tapi.GetHonorsAsync(cancellationToken);
            foreach ((int id, TornHonor honor) in data) {
                DataRow newRow = dtable.NewRow();
                newRow["HonorId"] = ProcessValue(id);
                newRow["HonorName"] = ProcessValue(honor.Name);
                newRow["HonorDescription"] = ProcessValue(honor.Description);
                newRow["HonorType"] = ProcessValue(honor.Type);
                newRow["Circulation"] = ProcessValue(honor.Circulation);
                newRow["Rarity"] = ProcessValue(honor.Rarity);
                dtable.Rows.Add(newRow);
            }

            return dtable;
        }

        /// <summary>
        /// Gets Torn medal data.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>A <see cref="=DataTable"/> containing the Torn medal data.</returns>
        public async Task<DataTable> GetMedalDataAsync(CancellationToken cancellationToken) {
            DataTable dtable = CreateTableSchema(new Dictionary<string, Type>() {
                { "MedalId", typeof(int) },
                { "MedalName", typeof(string) },
                { "MedalDescription", typeof(string) },
                { "MedalType", typeof(string) },
                { "Circulation", typeof(long) },
                { "Rarity", typeof(string) }
            });

            Dictionary<int, TornMedal> data = await tapi.GetMedalsAsync(cancellationToken);
            foreach ((int id, TornMedal medal) in data) {
                DataRow newRow = dtable.NewRow();
                newRow["MedalId"] = ProcessValue(id);
                newRow["MedalName"] = ProcessValue(medal.Name);
                newRow["MedalDescription"] = ProcessValue(medal.Description);
                newRow["MedalType"] = ProcessValue(medal.Type);
                newRow["Circulation"] = ProcessValue(medal.Circulation);
                newRow["Rarity"] = ProcessValue(medal.Rarity);
                dtable.Rows.Add(newRow);
            }

            return dtable;
        }

        /// <summary>
        /// Gets Torn organised crime data.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>A <see cref="=DataTable"/> containing the Torn organised crime data.</returns>
        public async Task<DataTable> GetOrganisedCrimeDataAsync(CancellationToken cancellationToken) {
            DataTable dtable = CreateTableSchema(new Dictionary<string, Type>() {
                { "OrganisedCrimeId", typeof(int) },
                { "CrimeName", typeof(string) },
                { "Members", typeof(int) },
                { "Duration", typeof(int) },
                { "CashMinimum", typeof(int) },
                { "CashMaximum", typeof(int) },
                { "RespectMinimum", typeof(int) },
                { "RespectMaximum", typeof(int) },
            });

            Dictionary<int, TornOrganisedCrime> data = await tapi.GetOrganisedCrimesAsync(cancellationToken);
            foreach ((int id, TornOrganisedCrime crime) in data) {
                DataRow newRow = dtable.NewRow();
                newRow["OrganisedCrimeId"] = ProcessValue(id);
                newRow["CrimeName"] = ProcessValue(crime.Name);
                newRow["Members"] = ProcessValue(crime.MembersNeeded);
                newRow["Duration"] = ProcessValue(crime.DurationHours);
                newRow["CashMinimum"] = ProcessValue(crime.MinimumCash);
                newRow["CashMaximum"] = ProcessValue(crime.MaximumCash);
                newRow["RespectMinimum"] = ProcessValue(crime.MinimumRespect);
                newRow["RespectMaximum"] = ProcessValue(crime.MaximumRespect);
                dtable.Rows.Add(newRow);
            }

            return dtable;
        }

        public void Dispose() => tapi.Dispose();

        // converts nulls to DBNull so they will insert correctly
        private object ProcessValue(object value) => value ?? DBNull.Value;

        /// <summary>
        /// Creates a data table with a schema defined by the supplised dictionary.
        /// </summary>
        /// <param name="coldefs">Defines the columns on the table.</param>
        /// <returns>An empty <see cref="DataTable"/> with the column schema configured.</returns>
        private DataTable CreateTableSchema(Dictionary<string, Type> coldefs) {
            var result = new DataTable();
            foreach ((string colName, Type colType) in coldefs) {
                result.Columns.Add(colName, colType);
            }
            return result;
        }
    }
}
