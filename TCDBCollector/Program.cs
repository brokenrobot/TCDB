using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using Amazon.Extensions.NETCore.Setup;
using Amazon.Runtime;
using Amazon.SecretsManager;
using Microsoft.Extensions.Logging;

namespace EonData.TornCity.Tcdb.Collector {
    public static class Program {
        public static void Main(string[] args) => CreateHostBuilder(args).Build().Run();

        public static IHostBuilder CreateHostBuilder(string[] args) => new HostBuilder()
                .UseSystemd()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureHostConfiguration((configBuilder) => {
                    configBuilder.AddEnvironmentVariables("DOTNET_");
                    if (args?.Any() ?? false) {
                        configBuilder.AddCommandLine(args);
                    }
                })
                .ConfigureAppConfiguration((configBuilder) => {
                    configBuilder.AddJsonFile("collector.json");
                })
                .ConfigureLogging((hostContext, logging) => {
                    logging.AddAWSProvider(new AWS.Logger.AWSLoggerConfig() {
                            LogGroup = "tcdbcollector",
                            DisableLogGroupCreation = true,
                            LogStreamNamePrefix = "dataimport",
                            LogStreamNameSuffix = Amazon.Util.EC2InstanceMetadata.InstanceId
                        }).AddConsole().SetMinimumLevel(LogLevel.Debug);
                })
                .ConfigureServices((hostContext, services) => {
                    services.AddDefaultAWSOptions(hostContext.Configuration.GetAWSOptions()).AddAWSService<IAmazonSecretsManager>()
                        .AddOptions().Configure<TcdbCollectorSecretsDetails>(hostContext.Configuration.GetSection("tcdb"))
                        .AddSingleton<ITcdbCollectorSecrets, TcdbCollectorSecretsAmazon>()
                        .AddHostedService<TcdbCollectorWorker>();
                });
    }
}