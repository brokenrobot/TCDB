﻿using System.Threading;
using System.Threading.Tasks;

namespace EonData.TornCity.Tcdb.Collector {
    internal interface ITcdbCollectorSecrets {
        Task<string> GetApiKeyAsync(CancellationToken cancellationToken);
        Task<string> GetDbConnectionStringAsync(CancellationToken cancellationToken);
    }
}