﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EonData.TornCity.Tcdb.Collector {
    class TcdbScheduleStartingEventArgs : EventArgs {
        public string ElementName { get; set; }
        public TimeSpan SinceLastRun { get; set; } = TimeSpan.MaxValue;
    }
}
