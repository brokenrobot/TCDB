﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace EonData.TornCity.Tcdb.Collector {
    internal class TcdbCollectorInstanceDetails {
        public string AppVersion { get; }

        public string DotNetVersion { get; }

        public string OperatingSystem { get; }

        public string Hostname { get; }

        public TcdbCollectorInstanceDetails() {
            AppVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            DotNetVersion = $"{RuntimeInformation.FrameworkDescription} {RuntimeInformation.ProcessArchitecture}";
            OperatingSystem = $"{RuntimeInformation.OSDescription} {RuntimeInformation.OSArchitecture}";
            Hostname = Dns.GetHostName();
        }
    }
}
