using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using EonData.TornCity.Api;
using EonData.TornCity.Tcdb.Collector;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Hosting.Systemd;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace EonData.TornCity.Tcdb.Collector {
    internal class TcdbCollectorWorker : BackgroundService {
        private const int KeepAliveIntervalSeconds = 10;

        private readonly ILogger<TcdbCollectorWorker> log;
        private readonly ITcdbCollectorSecrets secrets;
        private readonly ISystemdNotifier systemd;

        private DateTime LastKeepAlive;

        public TcdbCollectorWorker(ILogger<TcdbCollectorWorker> logger, ITcdbCollectorSecrets secretsManager, ISystemdNotifier sysdNotify) {
            systemd = sysdNotify;
            log = logger;
            secrets = secretsManager;
            log.LogDebug("created tcdb collector object.");
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken) {
            log.LogInformation("starting data collection operation.");
            try {
                // create the tcdb collection scheduler object
                await using var sched = new TcdbCollectorScheduleRunner(await secrets.GetApiKeyAsync(stoppingToken), await secrets.GetDbConnectionStringAsync(stoppingToken));
                await sched.Init(stoppingToken);
                // subscribe to events
                sched.Starting += OnScheduleStarting;
                sched.Completed += OnScheduleCompleted;

                // loop until the process is cancelled
                while (!stoppingToken.IsCancellationRequested) {
                    // notify the systemd watchdog
                    WatchdogKeepAlive();
                    // run a schedule (if one is ready)
                    await sched.CheckSchedules(stoppingToken);
                }
            }
            catch (Exception ex) {
                log.LogCritical(ex, "Uncaught exception during data collection operations...");
                throw;
            }

            log.LogInformation("stopping data collection operation.");
        }

        private void OnScheduleStarting(object _, TcdbScheduleStartingEventArgs args) {
            if (args.SinceLastRun == TimeSpan.MaxValue) {
                log.LogInformation("Starting the {TaskName} schedule task for the first time.", args.ElementName);
            }
            else {
                log.LogInformation("Starting the {TaskName} schedule task. It was last run {SinceLastRun} ago.", args.ElementName, args.SinceLastRun);
            }
        }

        private void OnScheduleCompleted(object _, TcdbScheduleCompletedEventArgs args) => log.LogInformation("Scheduled task {TaskName} has finished with a status of {TaskOutcome} in {TaskDuration}.", args.ElementName, args.CompletionStatus, args.TaskDuration);

        private void WatchdogKeepAlive() {
            if (LastKeepAlive == null || DateTime.Now - LastKeepAlive >= new TimeSpan(0, 0, KeepAliveIntervalSeconds)) {
                LastKeepAlive = DateTime.Now;
                systemd.Notify(new ServiceState("WATCHDOG=1"));
            }
        }
    }
}