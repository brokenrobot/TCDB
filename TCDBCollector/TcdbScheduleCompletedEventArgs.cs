﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EonData.TornCity.Tcdb.Collector {
    class TcdbScheduleCompletedEventArgs : EventArgs {
        public string ElementName { get; set; }
        public TaskStatus CompletionStatus { get; set; }
        public TimeSpan TaskDuration { get; set; }
    }
}
