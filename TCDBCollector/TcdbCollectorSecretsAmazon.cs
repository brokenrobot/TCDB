﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Amazon.SecretsManager;
using Amazon.SecretsManager.Model;
using Microsoft.Extensions.Options;

namespace EonData.TornCity.Tcdb.Collector {
    internal class TcdbCollectorSecretsAmazon : IDisposable, ITcdbCollectorSecrets {
        private readonly ILogger<TcdbCollectorSecretsAmazon> log;
        private readonly IAmazonSecretsManager secrets;
        private readonly TcdbCollectorSecretsDetails details;

        // store the values here after they have been retrieved once
        private string apikey;
        private string connectionString;

        public TcdbCollectorSecretsAmazon(ILogger<TcdbCollectorSecretsAmazon> logger, IOptions<TcdbCollectorSecretsDetails> secretsDetails, IAmazonSecretsManager secretsManager) {
            log = logger;
            details = secretsDetails.Value;
            secrets = secretsManager;
            log.LogDebug("created tcdb collector secrets object...");
        }

        public async Task<string> GetApiKeyAsync(CancellationToken cancellationToken) {
            if (apikey == null) {
                apikey = await GetSecretValueAsync(details.ApiKeySecret, cancellationToken);
            }

            return apikey;
        }

        public async Task<string> GetDbConnectionStringAsync(CancellationToken cancellationToken) {
            if (connectionString == null) {
                connectionString = await ReadConnectionStringAsync(cancellationToken);
            }

            return connectionString;
        }

        public void Dispose() {
            log?.LogDebug("Disposing of secrets object.");
            secrets.Dispose();
        }

        private class DbAuthDetails {
            public string Username { get; set; }
            public string Password { get; set; }
        }
        private class DbHostDetails {
            public string Hostname { get; set; }
            public int Port { get; set; }
        }

        private async Task<string> ReadConnectionStringAsync(CancellationToken cancellationToken) {
            DbAuthDetails dbAuth = await DeserializeSecretValueAsync<DbAuthDetails>(details.DbAuthSecret, cancellationToken);
            DbHostDetails dbHost = await DeserializeSecretValueAsync<DbHostDetails>(details.DbConnSecret, cancellationToken);
            var connstrBuilder = new Microsoft.Data.SqlClient.SqlConnectionStringBuilder() {
                UserID = dbAuth.Username,
                Password = dbAuth.Password,
                InitialCatalog = "tcdb",
                DataSource = (dbHost.Port == 0 || dbHost.Port == 1433) ? dbHost.Hostname : $"{dbHost.Hostname},{dbHost.Port}",
                ApplicationName = "TCDB data collector"
            };
            return connstrBuilder.ConnectionString;
        }

        private async Task<string> GetSecretValueAsync(string secretName, CancellationToken cancellationToken) {
            log.LogDebug($"Requesting AWS secret: {secretName}");
            var req = new GetSecretValueRequest() {
                SecretId = secretName
            };
            GetSecretValueResponse resp = await secrets.GetSecretValueAsync(req, cancellationToken);
            return resp.SecretString;
        }

        private async Task<T> DeserializeSecretValueAsync<T>(string secretName, CancellationToken cancellationToken) {
            string json = await GetSecretValueAsync(secretName, cancellationToken);
            await using var jstream = new MemoryStream(Encoding.Default.GetBytes(json));
            var jopt = new JsonSerializerOptions() {
                PropertyNameCaseInsensitive = true,
                AllowTrailingCommas = true
            };
            return await JsonSerializer.DeserializeAsync<T>(jstream, jopt, cancellationToken);
        }
    }
}
