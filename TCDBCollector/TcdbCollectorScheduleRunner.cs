﻿using EonData.TornCity.Api;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EonData.TornCity.Tcdb.Collector {
    /// <summary>
    /// Manages the <see cref="TcdbCollectorSchedule"/> objects.
    /// </summary>
    internal sealed class TcdbCollectorScheduleRunner : IAsyncDisposable {
        private const int ScheduleCheckWaitMilliseconds = 113;

        /// <summary>
        /// Triggered when a scheduled collection is starting.
        /// </summary>
        public event EventHandler<TcdbScheduleStartingEventArgs> Starting;

        /// <summary>
        /// Triggers when a scheduled collection has completed.
        /// </summary>
        public event EventHandler<TcdbScheduleCompletedEventArgs> Completed;

        private readonly TcdbCollectorApi tapi;
        private readonly TcdbCollectorDatabase tcdb;
        private readonly IEnumerable<TcdbCollectorSchedule> schedules;

        private Task curTask;
        private string curTaskElement;
        private DateTime curTaskStart;

        /// <summary>
        /// Creates a new TCDB collector schedule runner.
        /// </summary>
        /// <param name="apiKey">API key to use for the Torn API</param>
        /// <param name="dbConnString">Connection string for the TCDB database.</param>
        public TcdbCollectorScheduleRunner(string apiKey, string dbConnString) {
            // create api object
            tapi = new TcdbCollectorApi(apiKey);

            // create database object
            tcdb = new TcdbCollectorDatabase(dbConnString);

            // create schedules
            schedules = new List<TcdbCollectorSchedule>() {
                { new TcdbCollectorSchedule(tapi.GetGameStatDataAsync, "GameStats", TimeSpan.FromHours(12)) },
                { new TcdbCollectorSchedule(tapi.GetStockDataAsync, "Stocks", TimeSpan.FromMinutes(15)) },
                { new TcdbCollectorSchedule(tapi.GetItemDataAsync, "Items", TimeSpan.FromHours(1)) },
                { new TcdbCollectorSchedule(tapi.GetMedalDataAsync, "Medals", TimeSpan.FromHours(1)) },
                { new TcdbCollectorSchedule(tapi.GetHonorDataAsync, "Honors", TimeSpan.FromHours(1)) },
                { new TcdbCollectorSchedule(tapi.GetOrganisedCrimeDataAsync, "OrganisedCrimes", TimeSpan.FromHours(6)) },
                { new TcdbCollectorSchedule(tapi.GetGymDataAsync, "Gyms", TimeSpan.FromHours(6)) },
            };
        }

        public Task Init(CancellationToken cancellationToken) => tcdb.OpenAsync(cancellationToken);

        /// <summary>
        /// Attempts to run a schedule if one is ready and another one is not already running.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task CheckSchedules(CancellationToken cancellationToken) {
            // if the current task is completed or there isn't one set
            if (curTask?.IsCompleted ?? true) {
                // handle the task cleanup if one has just finished
                if (curTask != null) {
                    Completed?.Invoke(this, new TcdbScheduleCompletedEventArgs() {
                        ElementName = curTaskElement,
                        CompletionStatus = curTask.Status,
                        TaskDuration = DateTime.Now - curTaskStart
                    });

                    // rethrow any uncaught exceptions
                    curTask.Exception?.Handle((_) => false);

                    // get rid of the completed task object
                    curTask.Dispose();
                    curTask = null;
                }

                // check for schedules that are ready
                TcdbCollectorSchedule runSched = schedules.Where(s => s.IsReady).OrderByDescending(s => s.SinceLastRun).FirstOrDefault();

                // if a schedule is ready then run it
                if (runSched != null) {
                    Starting?.Invoke(this, new TcdbScheduleStartingEventArgs() {
                        ElementName = runSched.ElementName,
                        SinceLastRun = runSched.SinceLastRun
                    });

                    curTask = ProcessDataAsync(runSched, cancellationToken);
                    curTaskStart = DateTime.Now;
                    curTaskElement = runSched.ElementName;
                }
            }

            /* delay processing for <see cref="ScheduleCheckWaitMilliseconds"/> milliseconds or the current task completes. */

            // create a collection of tasks that contains a single delay task
            var waits = new List<Task>() {
                Task.Delay(ScheduleCheckWaitMilliseconds, cancellationToken)
            };

            // if there is a currently running data task add it to the collection
            if (curTask != null) {
                waits.Add(curTask);
            }

            // return a task taht completes when either the delay is over or the data collection ends.
            await Task.WhenAny(waits);
        }

        public async ValueTask DisposeAsync() {
            tapi.Dispose();
            await tcdb.DisposeAsync();
        }

        /// <summary>
        /// Processes data for a schedule.
        /// </summary>
        /// <param name="runSched">Schedule to process data for.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <remarks>This task is stored in <see cref="curTask"/> and represents the current data operation.</remarks>
        private async Task ProcessDataAsync(TcdbCollectorSchedule runSched, CancellationToken cancellationToken) {
            using DataTable data = await runSched.GetScheduleData(cancellationToken);
            await tcdb.ProcessData(runSched.ElementName, data, cancellationToken);
        }
    }
}
