﻿using EonData.TornCity.Api;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EonData.TornCity.Tcdb.Collector {
    /// <summary>
    /// TCDB data collection schedule.
    /// </summary>
    class TcdbCollectorSchedule {
        public const int MaxStaggerDelaySeconds = 90;
        /// <summary>
        /// Gets or sets the name of the element that this schedule collects data for. This should match the name of the landing table where the data is imported.
        /// </summary>
        public string ElementName { get; set; }

        /// <summary>
        /// Gets or sets the interval at which this schedule runs.
        /// </summary>
        public TimeSpan Interval { get; set; }

        /// <summary>
        /// Gets the last timme this schedule was run.
        /// </summary>
        public DateTime? LastRunTime { get; private set; }

        /// <summary>
        /// Gets the amount of time this execution will be delayed.
        /// </summary>
        public TimeSpan StaggerDelay { get; private set; }

        /// <summary>
        /// Gets whether or not this schedule is ready to be run.
        /// </summary>
        public bool IsReady => (LastRunTime == null) ? true : (DateTime.Now - LastRunTime >= (Interval + StaggerDelay));

        /// <summary>
        /// Gets the amount of time since this schedule was last run. Returns <see cref="TimeSpan.MaxValue"/> if it has never been run.
        /// </summary>
        public TimeSpan SinceLastRun {
            get {
                if (IsReady) {
                    // returns the maximum timespan value if this schedule has never been run
                    return (DateTime.Now - LastRunTime) ?? TimeSpan.MaxValue;
                }
                else {
                    return new TimeSpan(0);
                }
            }
        }

        private readonly Random rng = new Random();
        private readonly Func<CancellationToken, Task<DataTable>> collectorAction;

        /// <summary>
        /// Creates a new collection schedule that uses the supplied data collector function.
        /// </summary>
        /// <param name="collector">Data collector function used to retrieve data from the API.</param>
        public TcdbCollectorSchedule(Func<CancellationToken, Task<DataTable>> collector) => collectorAction = collector;

        /// <summary>
        /// Creates a new collection schedule that uses the supplied data collector function, element name, and interval.
        /// </summary>
        /// <param name="collector">Data collector function used to retrieve data from the API.</param>
        /// <param name="name">Name of the element that this schedule collects data for. This should match the name of the landing table where the data is imported.</param>
        /// <param name="wait">Interval at which this schedule runs.</param>
        public TcdbCollectorSchedule(Func<CancellationToken, Task<DataTable>> collector, string name, TimeSpan wait) : this(collector) {
            ElementName = name;
            Interval = wait;
        }

        /// <summary>
        /// Gets a DataTable that contains the data from the API request.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>DataTable with the data from the API.</returns>
        /// <remarks>This uses the function supplied in the constructor and also ensures that the timing variables are updated.</remarks>
        public Task<DataTable> GetScheduleData(CancellationToken cancellationToken) {
            LastRunTime = DateTime.Now;
            StaggerDelay = new TimeSpan(0, 0, rng.Next(MaxStaggerDelaySeconds));
            return collectorAction(cancellationToken);
        }
    }
}
