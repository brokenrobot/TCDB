﻿CREATE TABLE [stage].[OrganisedCrimes] (
    [OrganisedCrimeKey] BIGINT            IDENTITY (1, 1) NOT NULL,
    [OrganisedCrimeId]  INT            NOT NULL,
    [CrimeName]         NVARCHAR (50)  NULL,
    [Members]           INT            NULL,
    [Duration]          INT            NULL,
    [CashMinimum]       INT            NULL,
    [CashMaximum]       INT            NULL,
    [RespectMinimum]    INT            NULL,
    [RespectMaximum]    INT            NULL,
    [sys_isdeleted]     BIT            DEFAULT (0) NOT NULL,
    [sys_inserted]      DATETIME2 (7)  NOT NULL,
    [sys_updated]       DATETIME2 (7)  NULL,
    [sys_checksum]      VARBINARY (20) NULL
);

