﻿CREATE TABLE [stage].[Medals] (
    [MedalKey]         BIGINT            IDENTITY (1, 1) NOT NULL,
    [MedalId]          INT            NOT NULL,
    [MedalName]        NVARCHAR (100)  NULL,
    [MedalDescription] NVARCHAR (1000) NULL,
    [MedalType]        NVARCHAR (5)   NULL,
    [Circulation]      BIGINT         NULL,
    [Rarity]           NVARCHAR (100) NULL,
    [sys_isdeleted]    BIT            DEFAULT (0) NOT NULL,
    [sys_inserted]     DATETIME2 (7)  NOT NULL,
    [sys_updated]      DATETIME2 (7)  NULL,
    [sys_checksum]     VARBINARY (20) NULL
);

