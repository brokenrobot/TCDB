﻿CREATE TABLE [stage].[Stocks] (
    [StockKey]           BIGINT             IDENTITY (1, 1) NOT NULL,
    [StockId]            INT             NOT NULL,
    [StockName]          NVARCHAR (100)  NULL,
    [StockAcronym]       NVARCHAR (5)    NULL,
    [DirectorName]       NVARCHAR (100)   NULL,
    [CurrentPrice]       DECIMAL (18, 3) NULL,
    [MarketCap]          BIGINT          NULL,
    [TotalShares]        BIGINT          NULL,
    [Forecast]           NVARCHAR (25)   NULL,
    [Demand]             NVARCHAR (25)   NULL,
    [BenefitRequirement] BIGINT          NULL,
    [BenefitDescription] NVARCHAR (500)  NULL,
    [sys_isdeleted]      BIT             DEFAULT (0) NOT NULL,
    [sys_inserted]       DATETIME2 (7)   NOT NULL,
    [sys_updated]        DATETIME2 (7)   NULL,
    [sys_checksum]       VARBINARY (20)  NULL
);