﻿CREATE TABLE [stage].[Items] (
    [ItemKey]         BIGINT             IDENTITY (1, 1) NOT NULL,
    [ItemId]          INT             NOT NULL,
    [ItemName]        NVARCHAR (100)  NULL,
    [ItemDescription] NVARCHAR (2500) NULL,
    [ItemType]        NVARCHAR (50)   NULL,
    [WeaponType]      NVARCHAR (50)   NULL,
    [BuyPrice]        BIGINT          NULL,
    [SellPrice]       BIGINT          NULL,
    [MarketValue]     BIGINT          NULL,
    [Circulation]     BIGINT          NULL,
    [Effect]          NVARCHAR (1000) NULL,
    [Requirement]     NVARCHAR (1000) NULL,
    [ImageUrl]        NVARCHAR (1000) NULL,
    [sys_isdeleted]   BIT             DEFAULT (0) NOT NULL,
    [sys_inserted]    DATETIME2 (7)   NOT NULL,
    [sys_updated]     DATETIME2 (7)   NULL,
    [sys_checksum]    VARBINARY (20)  NULL
);

