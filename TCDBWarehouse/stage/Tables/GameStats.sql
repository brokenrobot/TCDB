﻿CREATE TABLE [stage].[GameStats] (
    [GameStatKey]   BIGINT             IDENTITY (1, 1) NOT NULL,
    [StatName]      NVARCHAR (50)   NOT NULL,
    [StatValue]     DECIMAL (32, 4) NULL,
    [sys_isdeleted] BIT             DEFAULT (0) NOT NULL,
    [sys_inserted]  DATETIME2 (7)   DEFAULT (sysdatetime()) NOT NULL,
    [sys_updated]   DATETIME2 (7)   NULL
);

