﻿CREATE TABLE [stage].[Honors] (
    [HonorKey]         BIGINT            IDENTITY (1, 1) NOT NULL,
    [HonorId]          INT            NOT NULL,
    [HonorName]        NVARCHAR (50)  NULL,
    [HonorDescription] NVARCHAR (250) NULL,
    [HonorType]        INT            NULL,
    [Circulation]      BIGINT         NULL,
    [Rarity]           NVARCHAR (100) NULL,
    [sys_isdeleted]    BIT            DEFAULT (0) NOT NULL,
    [sys_inserted]     DATETIME2 (7)  NOT NULL,
    [sys_updated]      DATETIME2 (7)  NULL,
    [sys_checksum]     VARBINARY (20) NULL
);

