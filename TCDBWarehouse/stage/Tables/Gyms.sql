﻿CREATE TABLE [stage].[Gyms] (
    [GymKey]        BIGINT              IDENTITY (1, 1) NOT NULL,
    [GymId]         INT              NOT NULL,
    [GymName]       NVARCHAR (50)    NULL,
    [Stage]         INT              NULL,
    [Cost]          INT              NULL,
    [Energy]        DECIMAL (22, 18) NULL,
    [Strength]      DECIMAL (22, 18) NULL,
    [Speed]         DECIMAL (22, 18) NULL,
    [Defense]       DECIMAL (22, 18) NULL,
    [Dexterity]     DECIMAL (22, 18) NULL,
    [Note]          NVARCHAR (1000)  NULL,
    [sys_isdeleted] BIT              DEFAULT (0) NOT NULL,
    [sys_inserted]  DATETIME2 (7)    NOT NULL,
    [sys_updated]   DATETIME2 (7)    NULL,
    [sys_checksum]  VARBINARY (20)   NULL
);

