﻿CREATE TABLE [landing].[Items] (
    [ItemId]          INT             NOT NULL,
    [ItemName]        NVARCHAR (100)  NULL,
    [ItemDescription] NVARCHAR (2500) NULL,
    [ItemType]        NVARCHAR (50)   NULL,
    [WeaponType]      NVARCHAR (50)   NULL,
    [BuyPrice]        BIGINT          NULL,
    [SellPrice]       BIGINT          NULL,
    [MarketValue]     BIGINT          NULL,
    [Circulation]     BIGINT          NULL,
    [Effect]          NVARCHAR (1000) NULL,
    [Requirement]     NVARCHAR (1000) NULL,
    [ImageUrl]        NVARCHAR (1000) NULL,
    [sys_inserted]    DATETIME2 (7)   DEFAULT (sysdatetime()) NOT NULL,
    [calc_checksum]   AS              ([dbo].[Create_Checksum](concat([ItemName],[ItemDescription],[ItemType],[WeaponType],CONVERT([nvarchar](max),[BuyPrice]),CONVERT([nvarchar](max),[SellPrice]),CONVERT([nvarchar](max),[MarketValue]),CONVERT([nvarchar](max),[Circulation]),[Effect],[Requirement],[ImageUrl])))
);



