﻿CREATE TABLE [landing].[OrganisedCrimes] (
    [OrganisedCrimeId] INT           NOT NULL,
    [CrimeName]        NVARCHAR (50) NULL,
    [Members]          INT           NULL,
    [Duration]         INT           NULL,
    [CashMinimum]      INT           NULL,
    [CashMaximum]      INT           NULL,
    [RespectMinimum]   INT           NULL,
    [RespectMaximum]   INT           NULL,
    [sys_inserted]     DATETIME2 (7) DEFAULT (sysdatetime()) NOT NULL,
    [calc_checksum]    AS            ([dbo].[Create_Checksum](concat([CrimeName],CONVERT([nvarchar](max),[Members]),CONVERT([nvarchar](max),[Duration]),CONVERT([nvarchar](max),[CashMinimum]),CONVERT([nvarchar](max),[CashMaximum]),CONVERT([nvarchar](max),[RespectMinimum]),CONVERT([nvarchar](max),[RespectMaximum]))))
);


