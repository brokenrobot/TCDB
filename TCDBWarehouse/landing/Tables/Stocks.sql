﻿CREATE TABLE [landing].[Stocks] (
    [StockId]            INT             NOT NULL,
    [StockName]          NVARCHAR (100)  NULL,
    [StockAcronym]       NVARCHAR (5)    NULL,
    [DirectorName]       NVARCHAR (100)   NULL,
    [CurrentPrice]       DECIMAL (18, 3) NULL,
    [MarketCap]          BIGINT          NULL,
    [TotalShares]        BIGINT          NULL,
    [Forecast]           NVARCHAR (25)   NULL,
    [Demand]             NVARCHAR (25)   NULL,
    [BenefitRequirement] BIGINT          NULL,
    [BenefitDescription] NVARCHAR (500)  NULL,
    [sys_inserted]       DATETIME2 (7)   DEFAULT (sysdatetime()) NOT NULL,
    [calc_checksum]      AS              ([dbo].[Create_Checksum](concat([StockName],[StockAcronym],[DirectorName],CONVERT([nvarchar](max),[CurrentPrice]),CONVERT([nvarchar](max),[MarketCap]),CONVERT([nvarchar](max),[TotalShares]),[Forecast],[Demand],CONVERT([nvarchar](max),[BenefitRequirement]),[BenefitDescription])))
);

