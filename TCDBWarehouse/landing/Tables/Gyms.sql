﻿CREATE TABLE [landing].[Gyms] (
    [GymId]         INT              NOT NULL,
    [GymName]       NVARCHAR (50)    NULL,
    [Stage]         INT              NULL,
    [Cost]          INT              NULL,
    [Energy]        DECIMAL (22, 18) NULL,
    [Strength]      DECIMAL (22, 18) NULL,
    [Speed]         DECIMAL (22, 18) NULL,
    [Defense]       DECIMAL (22, 18) NULL,
    [Dexterity]     DECIMAL (22, 18) NULL,
    [Note]          NVARCHAR (1000)  NULL,
    [sys_inserted]  DATETIME2 (7)    DEFAULT (sysdatetime()) NOT NULL,
    [calc_checksum] AS               ([dbo].[Create_Checksum](concat([GymName],CONVERT([nvarchar](max),[Stage]),CONVERT([nvarchar](max),[Cost]),CONVERT([nvarchar](max),[Energy]),CONVERT([nvarchar](max),[Strength]),CONVERT([nvarchar](max),[Speed]),CONVERT([nvarchar](max),[Defense]),CONVERT([nvarchar](max),[Dexterity]),[Note])))
);


