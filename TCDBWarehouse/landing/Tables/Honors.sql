﻿CREATE TABLE [landing].[Honors] (
    [HonorId]          INT            NOT NULL,
    [HonorName]        NVARCHAR (50)  NULL,
    [HonorDescription] NVARCHAR (250) NULL,
    [HonorType]        INT            NULL,
    [Circulation]      BIGINT         NULL,
    [Rarity]           NVARCHAR (100) NULL,
    [sys_inserted]     DATETIME2 (7)  DEFAULT (sysdatetime()) NOT NULL,
    [calc_checksum]    AS             ([dbo].[Create_Checksum](concat([HonorName],[HonorDescription],CONVERT([nvarchar](max),[HonorType]),[Rarity])))
);