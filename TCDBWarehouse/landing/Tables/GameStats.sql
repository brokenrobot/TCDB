﻿CREATE TABLE [landing].[GameStats] (
    [StatName]      NVARCHAR (50)   NOT NULL,
    [StatValue]     DECIMAL (32, 4) NULL,
    [sys_inserted]  DATETIME2 (7)   DEFAULT (sysdatetime()) NOT NULL
);