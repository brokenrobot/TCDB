﻿CREATE TABLE [landing].[Medals] (
    [MedalId]          INT            NOT NULL,
    [MedalName]        NVARCHAR (100)  NULL,
    [MedalDescription] NVARCHAR (1000) NULL,
    [MedalType]        NVARCHAR (5)   NULL,
    [Circulation]      BIGINT         NULL,
    [Rarity]           NVARCHAR (100) NULL,
    [sys_inserted]     DATETIME2 (7)  DEFAULT (sysdatetime()) NOT NULL,
    [calc_checksum]    AS             ([dbo].[Create_Checksum](concat([MedalName],[MedalDescription],[MedalType],CONVERT([nvarchar](max),[Circulation]),[Rarity])))
);



