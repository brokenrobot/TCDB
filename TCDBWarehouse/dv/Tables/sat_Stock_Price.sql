﻿CREATE TABLE [dv].[sat_Stock_Price] (
    [H_Stock_HK] VARBINARY(20) NOT NULL,
    [CurrentPrice]       DECIMAL (18, 3) NULL,
    [MarketCap]          BIGINT          NULL,
    [TotalShares]        BIGINT          NULL,
    [Forecast]           NVARCHAR (25)   NULL,
    [Demand]             NVARCHAR (25)   NULL,
    [S_Stock_Price_HDIFF] VARBINARY(20) NOT NULL,
	[S_Stock_Price_LDTS] DATETIME2(7) NOT NULL,
	CONSTRAINT [PK_sat_Stock_Price] PRIMARY KEY CLUSTERED ([H_Stock_HK] ASC, [S_Stock_Price_LDTS] ASC),
	CONSTRAINT [FK_sat_Stock_Price_hub_Stock] FOREIGN KEY ([H_Stock_HK]) REFERENCES [dv].[hub_Stock]([H_Stock_HK])
);