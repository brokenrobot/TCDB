﻿CREATE TABLE [dv].[sat_Item] (
	[H_Item_HK] VARBINARY(20) NOT NULL,
    [ItemDescription] NVARCHAR (2500) NULL,
    [ItemType]        NVARCHAR (50)   NULL,
    [WeaponType]      NVARCHAR (50)   NULL,
    [Effect]          NVARCHAR (1000) NULL,
    [Requirement]     NVARCHAR (1000) NULL,
    [ImageUrl]        NVARCHAR (1000) NULL,
    [sys_isdeleted]   BIT NOT NULL,
    [sys_stagekey] BIGINT NULL,
    [S_Item_HDIFF] VARBINARY(20) NOT NULL,
    [S_Item_LDTS]     DATETIME2 (7)   NOT NULL
    CONSTRAINT [PK_sat_Item] PRIMARY KEY CLUSTERED ([H_Item_HK] ASC, [S_Item_LDTS] ASC),
    CONSTRAINT [FK_sat_Item_hub_Item] FOREIGN KEY ([H_Item_HK]) REFERENCES [dv].[hub_Item] ([H_Item_HK])
)
