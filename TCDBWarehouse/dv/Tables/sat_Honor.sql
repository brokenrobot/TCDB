﻿CREATE TABLE [dv].[sat_Honor] (
	[H_Honor_HK] VARBINARY(20) NOT NULL,
    [HonorDescription] NVARCHAR (250) NULL,
    [HonorType]        INT            NULL,
    [Circulation]      BIGINT         NULL,
    [Rarity]           NVARCHAR (100) NULL,
	[sys_isdeleted] BIT DEFAULT 0 NOT NULL,
	[sys_stagekey] BIGINT NULL,
	[S_Honor_LDTS] DATETIME2(7) NOT NULL,
	[S_Honor_HDIFF] VARBINARY(20) NOT NULL,
	CONSTRAINT [PK_sat_Honor] PRIMARY KEY CLUSTERED ([H_Honor_HK] ASC, [S_Honor_LDTS] ASC),
	CONSTRAINT [FK_sat_Honor_hub_Honor] FOREIGN KEY ([H_Honor_HK]) REFERENCES [dv].[hub_Honor]([H_Honor_HK])
);