﻿CREATE TABLE [dv].[hub_OrganisedCrime] (
	[H_OrganisedCrime_HK] VARBINARY(20) NOT NULL,
	[OrganisedCrimeId_BK] INT NULL,
	[CrimeName_BK] NVARCHAR(50) NULL,
	[H_OrganisedCrime_LDTS] DATETIME2(7) NOT NULL,
	CONSTRAINT [PK_hub_OrganisedCrime] PRIMARY KEY CLUSTERED ([H_OrganisedCrime_HK]),
	CONSTRAINT [AK_hub_OrganisedCrime] UNIQUE ([OrganisedCrimeId_BK], [CrimeName_BK])
);