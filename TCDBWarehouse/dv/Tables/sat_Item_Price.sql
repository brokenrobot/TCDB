﻿CREATE TABLE [dv].[sat_Item_Price] (
    [H_Item_HK] VARBINARY(20) NOT NULL,
    [BuyPrice]        BIGINT          NULL,
    [SellPrice]       BIGINT          NULL,
    [MarketValue]     BIGINT          NULL,
    [Circulation]     BIGINT          NULL,
    [S_Item_Price_HDIFF] VARBINARY(20) NOT NULL,
    [S_Item_Price_LDTS]     DATETIME2 (7)   NOT NULL
    CONSTRAINT [PK_sat_Item_Price] PRIMARY KEY CLUSTERED ([H_Item_HK], [S_Item_Price_LDTS] ASC),
    CONSTRAINT [FK_sat_Item_Price_hub_Item] FOREIGN KEY ([H_Item_HK]) REFERENCES [dv].[hub_Item] ([H_Item_HK])
);