﻿CREATE TABLE [dv].[sat_Medal] (
	[H_Medal_HK] VARBINARY(20) NOT NULL,
    [MedalDescription] NVARCHAR (1000) NULL,
    [MedalType]        NVARCHAR (5)   NULL,
    [Circulation]      BIGINT         NULL,
    [Rarity]           NVARCHAR (100) NULL,
	[sys_isdeleted] BIT DEFAULT 0 NOT NULL,
	[sys_stagekey] BIGINT NULL,
	[S_Medal_LDTS] DATETIME2(7) NOT NULL,
	[S_Medal_HDIFF] VARBINARY(20) NOT NULL,
	CONSTRAINT [PK_sat_Medal] PRIMARY KEY CLUSTERED ([H_Medal_HK] ASC, [S_Medal_LDTS] ASC),
	CONSTRAINT [FK_sat_Medal_hub_Medal] FOREIGN KEY ([H_Medal_HK]) REFERENCES [dv].[hub_Medal]([H_Medal_HK])
);