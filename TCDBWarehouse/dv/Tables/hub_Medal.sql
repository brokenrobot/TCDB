﻿CREATE TABLE [dv].[hub_Medal] (
	[H_Medal_HK] VARBINARY(20) NOT NULL,
	[MedalId_BK] INT NULL,
	[MedalName_BK] NVARCHAR(100) NULL,
	[H_Medal_LDTS] DATETIME2(7) NOT NULL,
	CONSTRAINT [PK_hub_Medal] PRIMARY KEY CLUSTERED ([H_Medal_HK]),
	CONSTRAINT [AK_hub_Medal] UNIQUE ([MedalId_BK], [MedalName_BK])
);