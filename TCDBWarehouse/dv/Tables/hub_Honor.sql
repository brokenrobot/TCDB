﻿CREATE TABLE [dv].[hub_Honor] (
	[H_Honor_HK] VARBINARY(20) NOT NULL,
	[HonorId_BK] INT NULL,
	[HonorName_BK] NVARCHAR(50) NULL,
	[H_Honor_LDTS] DATETIME2(7) NOT NULL,
	CONSTRAINT [PK_hub_Honor] PRIMARY KEY CLUSTERED ([H_Honor_HK]),
	CONSTRAINT [AK_hub_Honor] UNIQUE ([HonorId_BK], [HonorName_BK])
);