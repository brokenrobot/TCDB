﻿CREATE TABLE [dv].[sat_Gym] (
    [H_Gym_HK] VARBINARY(20) NOT NULL,
    [Stage]         INT              NULL,
    [Cost]          INT              NULL,
    [Energy]        DECIMAL (22, 18) NULL,
    [Strength]      DECIMAL (22, 18) NULL,
    [Speed]         DECIMAL (22, 18) NULL,
    [Defense]       DECIMAL (22, 18) NULL,
    [Dexterity]     DECIMAL (22, 18) NULL,
    [Note]          NVARCHAR (1000)  NULL,
	[sys_isdeleted] BIT NOT NULL,
    [sys_stagekey] BIGINT NULL,
	[S_Gym_LDTS] DATETIME2(7) NOT NULL,
    [S_Gym_HDIFF] VARBINARY(20) NOT NULL,
	CONSTRAINT [PK_sat_Gym] PRIMARY KEY CLUSTERED ([H_Gym_HK] ASC, [S_Gym_LDTS] ASC),
	CONSTRAINT [FK_sat_Gym_hub_Gym] FOREIGN KEY ([H_Gym_HK]) REFERENCES [dv].[hub_Gym]([H_Gym_HK])
);