﻿CREATE TABLE [dv].[sat_OrganisedCrime] (
    [H_OrganisedCrime_HK] VARBINARY(20) NOT NULL,
    [Members]           INT            NULL,
    [Duration]          INT            NULL,
    [CashMinimum]       INT            NULL,
    [CashMaximum]       INT            NULL,
    [RespectMinimum]    INT            NULL,
    [RespectMaximum]    INT            NULL,
	[sys_isdeleted] BIT NOT NULL,
    [sys_stagekey] BIGINT NULL,
	[S_OrganisedCrime_LDTS] DATETIME2(7) NOT NULL,
    [S_OrganisedCrime_HDIFF] VARBINARY(20) NOT NULL,
	CONSTRAINT [PK_sat_OrganisedCrime] PRIMARY KEY CLUSTERED ([H_OrganisedCrime_HK] ASC, [S_OrganisedCrime_LDTS] ASC),
	CONSTRAINT [FK_sat_OrganisedCrime_hub_OrganisedCrime] FOREIGN KEY ([H_OrganisedCrime_HK]) REFERENCES [dv].[hub_OrganisedCrime]([H_OrganisedCrime_HK])
);