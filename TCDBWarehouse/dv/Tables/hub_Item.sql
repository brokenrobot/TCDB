﻿CREATE TABLE [dv].[hub_Item] (
    [H_Item_HK] VARBINARY(20) NOT NULL,
    [ItemId_BK] BIGINT NULL,
    [ItemName_BK] NVARCHAR(100) NULL,
    [H_Item_LDTS] DATETIME2(7) NOT NULL
    CONSTRAINT [PK_hub_Item] PRIMARY KEY CLUSTERED ([H_Item_HK])
    CONSTRAINT [AK_hub_Item] UNIQUE ([ItemId_BK],[ItemName_BK])
);