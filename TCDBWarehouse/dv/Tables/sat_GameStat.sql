﻿CREATE TABLE [dv].[sat_GameStat] (
	[H_GameStat_HK] VARBINARY(20) NOT NULL,
    [StatValue]     DECIMAL (32, 4) NULL,
	[sys_isdeleted] BIT NOT NULL,
	[sys_stagekey] BIGINT NULL,
	[S_GameStat_HDIFF] VARBINARY(20) NOT NULL,
	[S_GameStat_LDTS] DATETIME2(7) NOT NULL,
	CONSTRAINT [PK_sat_GameStat] PRIMARY KEY CLUSTERED ([H_GameStat_HK] ASC, [S_GameStat_LDTS] ASC),
	CONSTRAINT [FK_sat_GameStat_hub_GameStat] FOREIGN KEY ([H_GameStat_HK]) REFERENCES [dv].[hub_GameStat]([H_GameStat_HK])
);