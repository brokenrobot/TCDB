﻿CREATE TABLE [dv].[sat_Stock] (
    [H_Stock_HK] VARBINARY(20) NOT NULL,
    [StockName]          NVARCHAR (100)  NULL,
    [DirectorName]       NVARCHAR (100)   NULL,
    [BenefitRequirement] BIGINT          NULL,
    [BenefitDescription] NVARCHAR (500)  NULL,
	[sys_isdeleted]   BIT NOT NULL,
    [sys_stagekey] BIGINT NULL,
    [S_Stock_HDIFF] VARBINARY(20) NOT NULL,
	[S_Stock_LDTS] DATETIME2(7) NOT NULL,
	CONSTRAINT [PK_sat_Stock] PRIMARY KEY CLUSTERED ([H_Stock_HK] ASC, [S_Stock_LDTS] ASC),
	CONSTRAINT [FK_sat_Stock_hub_Stock] FOREIGN KEY ([H_Stock_HK]) REFERENCES [dv].[hub_Stock]([H_Stock_HK])
);