﻿CREATE TABLE [dv].[hub_Stock] (
	[H_Stock_HK] VARBINARY(20) NOT NULL,
	[StockId_BK] INT NULL,
	[StockAcronym_BK] NVARCHAR(5) NULL,
	[H_Stock_LDTS] DATETIME2(7) NOT NULL,
	CONSTRAINT [PK_hub_Stock] PRIMARY KEY CLUSTERED ([H_Stock_HK]),
	CONSTRAINT [AK_hub_Stock] UNIQUE ([StockId_BK], [StockAcronym_BK])
);