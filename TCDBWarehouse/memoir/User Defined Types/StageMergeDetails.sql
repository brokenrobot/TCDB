﻿CREATE TYPE [memoir].[StageMergeDetails] AS TABLE (
	[ElementKey] BIGINT NULL,
	[MergeAction] NVARCHAR(10) NULL,
	[EntryIsDeleted] BIT NULL,
	[DataLoadedTimestamp] DATETIME2(7) NULL
);