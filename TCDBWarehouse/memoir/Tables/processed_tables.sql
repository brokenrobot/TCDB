﻿CREATE TABLE [memoir].[processed_tables] (
	[DataImportLogKey] BIGINT IDENTITY(1,1) NOT NULL,
	[DataImportLogTimestamp] DATETIME2(7) CONSTRAINT [DF_log_data_import_timestamp] DEFAULT (SYSDATETIME()),
	[ImportStage] NVARCHAR(25) NULL,
	[TableName] NVARCHAR(100) NULL,
	[RowsInserted] INT NULL,
	[RowsUpdated] INT NULL,
	[RowsDeleted] INT NULL,
	[CollectorLogKey] BIGINT NULL
)