﻿CREATE TABLE [memoir].[staged_changes] (
	[StagedChangeLogKey] BIGINT IDENTITY(1,1) NOT NULL,
	[StagedChangeLogTimestamp] DATETIME2(7) CONSTRAINT [DF_log_staged_changes_timestamp] DEFAULT (SYSDATETIME()) NOT NULL,
	[TableName] NVARCHAR(25) NULL,
	[ElementKey] BIGINT NULL,
	[MergeAction] NVARCHAR(10) NULL,
	[EntryIsDeleted] BIT NULL,
	[EntryTimestamp] DATETIME2(7) NULL,
	[CollectorLogKey] BIGINT NULL
);