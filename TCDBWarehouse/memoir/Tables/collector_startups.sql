﻿CREATE TABLE [memoir].[collector_startups] (
	[CollectorLogKey] BIGINT IDENTITY(1,1) NOT NULL,
	[CollectorLogTimestamp] DATETIME2(7) CONSTRAINT [DF_log_collectors_timestamp] DEFAULT (SYSDATETIME()),
	[InstanceHostname] NVARCHAR(2000) NULL,
	[CollectorVersion] NVARCHAR(50) NULL,
	[DotNetVersion] NVARCHAR(50) NULL,
	[OperatingSystem] NVARCHAR(500) NULL
);