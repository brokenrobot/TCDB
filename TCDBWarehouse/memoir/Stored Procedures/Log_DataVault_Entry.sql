﻿CREATE PROCEDURE [memoir].[Log_DataVault_Entry] (
	@TableName NVARCHAR(100),
	@RowsInserted INT,
	@CollectorLogKey BIGINT
)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO [memoir].[processed_tables] (
		[ImportStage],
		[TableName],
		[RowsInserted],
		[CollectorLogKey]
	)
	VALUES (
		'datavault',
		@TableName,
		@RowsInserted,
		@CollectorLogKey
	);
END;