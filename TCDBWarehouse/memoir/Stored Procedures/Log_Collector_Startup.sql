﻿CREATE PROCEDURE [memoir].[Log_Collector_Startup] (
	@Hostname NVARCHAR(2000),
	@CollectorVersion NVARCHAR(50),
	@DotNetVersion NVARCHAR(50),
	@OperatingSystem NVARCHAR(500),
	@CollectorLogKey BIGINT OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO [memoir].[collector_startups] (
		[InstanceHostname],
		[CollectorVersion],
		[DotNetVersion],
		[OperatingSystem]
	)
	VALUES (
		@Hostname,
		@CollectorVersion,
		@DotNetVersion,
		@OperatingSystem
	);

	SET @CollectorLogKey = SCOPE_IDENTITY();
END;