﻿CREATE PROCEDURE [memoir].[Log_Stage_Merge] (
	@TableName NVARCHAR(100),
	@Details [memoir].[StageMergeDetails] READONLY,
	@CollectorLogKey BIGINT
)
AS
BEGIN
	SET NOCOUNT ON;

	-- log the number of rows affected by the merge.
	INSERT INTO [memoir].[processed_tables] (
		[ImportStage],
		[TableName],
		[RowsInserted],
		[RowsUpdated],
		[RowsDeleted],
		[CollectorLogKey]
	)
	SELECT TOP 1 'stage',
		@TableName,
		[INSERT],
		[UPDATE],
		[DELETE],
		@CollectorLogKey
	FROM (
		SELECT pvt.[INSERT], pvt.[UPDATE], pvt.[DELETE], 1 AS ord
		FROM @Details AS d
		PIVOT (
			COUNT([ElementKey]) FOR [MergeAction] IN ([INSERT],[UPDATE],[DELETE])
		) AS pvt
		-- if the above pivot query returns no rows then use the values below to ensure at least 1 row is returned for entry into the loggign table.
		UNION
		SELECT 0 AS [INSERT], 0 AS [UPDATE], 0 AS [DELETE], 100 AS ord
	) AS l
	ORDER BY [ord] ASC;

	-- log each change that was merged into staging
	INSERT INTO [memoir].[staged_changes] (
		[TableName],
		[ElementKey],
		[MergeAction],
		[EntryIsDeleted],
		[EntryTimestamp],
		[CollectorLogKey]
	)
	SELECT @TableName,
		d.[ElementKey],
		d.[MergeAction],
		d.[EntryIsDeleted],
		d.[DataLoadedTimestamp],
		@CollectorLogKey
	FROM @Details AS d;
END;