﻿CREATE PROCEDURE [memoir].[Log_Landing_Count] (
	@TableName NVARCHAR(100),
	@RowsExported INT,
	@CollectorLogKey BIGINT
)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO [memoir].[processed_tables] (
		[ImportStage],
		[TableName],
		[RowsInserted],
		[CollectorLogKey]
	)
	VALUES (
		'landing',
		@TableName,
		@RowsExported,
		@CollectorLogKey
	);
END;