﻿CREATE PROCEDURE [dbo].[Process_Honors_data] (
	@CollectorLogKey BIGINT
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @stage_details [memoir].[StageMergeDetails];

	-- move landed data into the staging area
	MERGE INTO [stage].[Honors] AS s
	USING [landing].[Honors] AS l
	ON s.HonorId=l.HonorId
	WHEN MATCHED AND (l.calc_checksum!=s.sys_checksum OR s.sys_isdeleted=1)
	THEN UPDATE SET
		s.HonorName=l.HonorName,
		s.HonorDescription=l.HonorDescription,
		s.HonorType=l.HonorType,
		s.Circulation=l.Circulation,
		s.Rarity=l.Rarity,
		s.sys_isdeleted=0,
		s.sys_checksum=l.calc_checksum,
		s.sys_updated=l.sys_inserted
	WHEN NOT MATCHED BY TARGET
	THEN INSERT (
			HonorId,
			HonorName,
			HonorDescription,
			HonorType,
			Circulation,
			Rarity,
			sys_checksum,
			sys_inserted
		)
		VALUES (
			l.HonorId,
			l.HonorName,
			l.HonorDescription,
			l.HonorType,
			l.Circulation,
			l.Rarity,
			l.calc_checksum,
			l.sys_inserted
		)
	WHEN NOT MATCHED BY SOURCE AND (s.sys_isdeleted=0)
	THEN UPDATE SET
		s.sys_isdeleted=1,
		s.sys_updated=SYSDATETIME()
	OUTPUT INSERTED.HonorKey,
		$action,
		INSERTED.sys_isdeleted,
		COALESCE(INSERTED.sys_updated, INSERTED.sys_inserted)
	INTO @stage_details (
		[ElementKey],
		[MergeAction],
		[EntryIsDeleted],
		[DataLoadedTimestamp]
	);

	EXEC [memoir].[Log_Stage_Merge] 'Honors', @stage_details, @CollectorLogKey;

	-- capture values used for dv procesisng
	DECLARE @change_details TABLE (
		dv_hashkey VARBINARY(20) NOT NULL,
		dv_hashdiff VARBINARY(20) NOT NULL,
		dv_timestamp DATETIME2(7) NOT NULL,
		HonorKey BIGINT NOT NULL,
		HonorId INT NOT NULL,
		HonorName NVARCHAR(50) NOT NULL
	);

	INSERT INTO @change_details (dv_hashkey, dv_hashdiff, dv_timestamp, HonorKey, HonorId, HonorName)
	SELECT dbo.Create_Checksum(CONCAT(CAST(s.[HonorId] AS NVARCHAR(MAX)), s.[HonorName])) AS dv_hashkey,
		dbo.Create_Checksum(CONCAT(CAST(s.[HonorDescription] AS NVARCHAR(MAX)), s.[HonorType], s.[Circulation], s.[Rarity], s.[sys_isdeleted])) as dv_hashdiff,
		COALESCE(s.[sys_updated], s.[sys_inserted]) AS dv_timestamp,
		s.[HonorKey],
		s.[HonorId],
		s.[HonorName]
	FROM [stage].[Honors] AS s;

	-- insert new hub entries
	INSERT INTO [dv].[hub_Honor] (
		[H_Honor_HK],
		[HonorId_BK],
		[HonorName_BK],
		[H_Honor_LDTS]
	)
	SELECT chgd.[dv_hashkey],
		chgd.HonorId,
		chgd.HonorName,
		chgd.dv_timestamp
	FROM @change_details AS chgd
	WHERE NOT EXISTS (
			SELECT *
			FROM [dv].[hub_Honor] AS dvh
			WHERE dvh.[H_Honor_HK] = chgd.[dv_hashkey]
		);

	EXEC [memoir].[Log_DataVault_Entry] 'hub_Honor', @@ROWCOUNT, @CollectorLogKey;

	-- insert new data into satellite
	INSERT INTO [dv].[sat_Honor] (
		[H_Honor_HK],
		[HonorDescription],
		[HonorType],
		[Circulation],
		[Rarity],
		[sys_isdeleted],
		[sys_stagekey],
		[S_Honor_HDIFF],
		[S_Honor_LDTS]
	)
	SELECT chgd.[dv_hashkey],
		s.[HonorDescription],
		s.[HonorType],
		s.[Circulation],
		s.[Rarity],
		s.[sys_isdeleted],
		s.[HonorKey],
		chgd.[dv_hashdiff],
		chgd.[dv_timestamp]
	FROM @change_details AS chgd
	LEFT JOIN (
		SELECT [H_Honor_HK], [S_Honor_HDIFF]
		FROM (
			SELECT [H_Honor_HK], [S_Honor_HDIFF],
				ROW_NUMBER() OVER(PARTITION BY [H_Honor_HK] ORDER BY [S_Honor_LDTS] DESC) AS rnum
			FROM [dv].[sat_Honor]
		) as i
		WHERE rnum=1
	) AS sat_latest ON sat_latest.[H_Honor_HK] = chgd.[dv_hashkey]
	INNER JOIN [stage].[Honors] AS s ON s.[HonorKey] = chgd.[HonorKey]
	WHERE sat_latest.[H_Honor_HK] IS NULL OR sat_latest.[S_Honor_HDIFF] != chgd.[dv_hashdiff];

	EXEC [memoir].[Log_DataVault_Entry] 'sat_Honor', @@ROWCOUNT, @CollectorLogKey;
END;