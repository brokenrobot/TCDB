﻿CREATE PROCEDURE [dbo].[Process_OrganisedCrimes_data] (
	@CollectorLogKey BIGINT
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @stage_details [memoir].[StageMergeDetails];

	-- move landed data into the staging area
	MERGE INTO [stage].[OrganisedCrimes] AS s
	USING [landing].[OrganisedCrimes] AS l
	ON s.OrganisedCrimeId=l.OrganisedCrimeId
	WHEN MATCHED AND (l.calc_checksum!=s.sys_checksum OR s.sys_isdeleted=1)
	THEN UPDATE SET
		s.CrimeName=l.CrimeName,
		s.Members=l.Members,
		s.Duration=l.Duration,
		s.CashMinimum=l.CashMinimum,
		s.CashMaximum=l.CashMaximum,
		s.RespectMinimum=l.RespectMinimum,
		s.RespectMaximum=l.RespectMaximum,
		s.sys_isdeleted=0,
		s.sys_checksum=l.calc_checksum,
		s.sys_updated=l.sys_inserted
	WHEN NOT MATCHED BY TARGET
	THEN INSERT (
			OrganisedCrimeId,
			CrimeName,
			Members,
			Duration,
			CashMinimum,
			CashMaximum,
			RespectMinimum,
			RespectMaximum,
			sys_checksum,
			sys_inserted
		)
		VALUES (
			l.OrganisedCrimeId,
			l.CrimeName,
			l.Members,
			l.Duration,
			l.CashMinimum,
			l.CashMaximum,
			l.RespectMinimum,
			l.RespectMaximum,
			l.calc_checksum,
			l.sys_inserted
		)
	WHEN NOT MATCHED BY SOURCE AND (s.sys_isdeleted=0)
	THEN UPDATE SET
		s.sys_isdeleted=1,
		s.sys_updated=SYSDATETIME()
	OUTPUT INSERTED.OrganisedCrimeKey,
		$action,
		INSERTED.sys_isdeleted,
		COALESCE(INSERTED.sys_updated, INSERTED.sys_inserted)
	INTO @stage_details (
		[ElementKey],
		[MergeAction],
		[EntryIsDeleted],
		[DataLoadedTimestamp]
	);

	EXEC [memoir].[Log_Stage_Merge] 'OrganisedCrimes', @stage_details, @CollectorLogKey;

	-- capture values used for dv procesisng
	DECLARE @change_details TABLE (
		dv_hashkey VARBINARY(20) NOT NULL,
		dv_hashdiff VARBINARY(20) NOT NULL,
		dv_timestamp DATETIME2(7) NOT NULL,
		OrganisedCrimeKey BIGINT NOT NULL,
		OrganisedCrimeId INT NOT NULL,
		CrimeName NVARCHAR(50) NOT NULL
	);

	INSERT INTO @change_details (dv_hashkey, dv_hashdiff, dv_timestamp, OrganisedCrimeKey, OrganisedCrimeId, CrimeName)
	SELECT dbo.Create_Checksum(CONCAT(CAST(s.[OrganisedCrimeId] AS NVARCHAR(MAX)), s.[CrimeName])) AS dv_hashkey,
		dbo.Create_Checksum(CONCAT(CAST(s.[Members] AS NVARCHAR(MAX)), s.[Duration], s.[CashMinimum], s.[CashMaximum], s.[RespectMinimum], s.[RespectMaximum], s.[sys_isdeleted])) as dv_hashdiff,
		COALESCE(s.[sys_updated], s.[sys_inserted]) AS dv_timestamp,
		s.[OrganisedCrimeKey],
		s.[OrganisedCrimeId],
		s.[CrimeName]
	FROM [stage].[OrganisedCrimes] AS s;

	-- insert new hub entries
	INSERT INTO [dv].[hub_OrganisedCrime] (
		[H_OrganisedCrime_HK],
		[OrganisedCrimeId_BK],
		[CrimeName_BK],
		[H_OrganisedCrime_LDTS]
	)
	SELECT chgd.[dv_hashkey],
		chgd.OrganisedCrimeId,
		chgd.CrimeName,
		chgd.dv_timestamp
	FROM @change_details AS chgd
	WHERE NOT EXISTS (
			SELECT *
			FROM [dv].[hub_OrganisedCrime] AS dvh
			WHERE dvh.[H_OrganisedCrime_HK] = chgd.[dv_hashkey]
		);

	EXEC [memoir].[Log_DataVault_Entry] 'hub_OrganisedCrime', @@ROWCOUNT, @CollectorLogKey;

	-- insert new data into satellite
	INSERT INTO [dv].[sat_OrganisedCrime] (
		[H_OrganisedCrime_HK],
		[Members],
		[Duration],
		[CashMinimum],
		[CashMaximum],
		[RespectMinimum],
		[RespectMaximum],
		[sys_isdeleted],
		[sys_stagekey],
		[S_OrganisedCrime_HDIFF],
		[S_OrganisedCrime_LDTS]
	)
	SELECT chgd.[dv_hashkey],
		s.[Members],
		s.[Duration],
		s.[CashMinimum],
		s.[CashMaximum],
		s.[RespectMinimum],
		s.[RespectMaximum],
		s.[sys_isdeleted],
		s.[OrganisedCrimeKey],
		chgd.[dv_hashdiff],
		chgd.[dv_timestamp]
	FROM @change_details AS chgd
	LEFT JOIN (
		SELECT [H_OrganisedCrime_HK], [S_OrganisedCrime_HDIFF]
		FROM (
			SELECT [H_OrganisedCrime_HK], [S_OrganisedCrime_HDIFF],
				ROW_NUMBER() OVER(PARTITION BY [H_OrganisedCrime_HK] ORDER BY [S_OrganisedCrime_LDTS] DESC) AS rnum
			FROM [dv].[sat_OrganisedCrime]
		) as i
		WHERE rnum=1
	) AS sat_latest ON sat_latest.[H_OrganisedCrime_HK] = chgd.[dv_hashkey]
	INNER JOIN [stage].[OrganisedCrimes] AS s ON s.[OrganisedCrimeKey] = chgd.[OrganisedCrimeKey]
	WHERE sat_latest.[H_OrganisedCrime_HK] IS NULL OR sat_latest.[S_OrganisedCrime_HDIFF] != chgd.[dv_hashdiff];

	EXEC [memoir].[Log_DataVault_Entry] 'sat_OrganisedCrime', @@ROWCOUNT, @CollectorLogKey;
END;