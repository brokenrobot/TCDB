﻿CREATE PROCEDURE [dbo].[Process_Stocks_data] (
	@CollectorLogKey BIGINT
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @stage_details [memoir].[StageMergeDetails];

	-- merge data from the landing table to staging
	MERGE INTO [stage].[Stocks] AS s
	USING [landing].[Stocks] AS l
	ON l.StockId=s.StockId
	WHEN MATCHED AND (l.calc_checksum!=s.sys_checksum OR s.sys_isdeleted=1)
	THEN UPDATE	SET
		s.StockName=l.StockName,
		s.StockAcronym=l.StockAcronym,
		s.DirectorName=l.DirectorName,
		s.CurrentPrice=l.CurrentPrice,
		s.MarketCap=l.MarketCap,
		s.TotalShares=l.TotalShares,
		s.Forecast=l.Forecast,
		s.Demand=l.Demand,
		s.BenefitRequirement=l.BenefitRequirement,
		s.BenefitDescription=l.BenefitDescription,
		s.sys_isdeleted=0,
		s.sys_checksum=l.calc_checksum,
		s.sys_updated=l.sys_inserted
	WHEN NOT MATCHED BY TARGET
	THEN INSERT (
			StockId,
			StockName,
			StockAcronym,
			DirectorName,
			CurrentPrice,
			MarketCap,
			TotalShares,
			Forecast,
			Demand,
			BenefitRequirement,
			BenefitDescription,
			sys_checksum,
			sys_inserted
		)
		VALUES (
			l.StockId,
			l.StockName,
			l.StockAcronym,
			l.DirectorName,
			l.CurrentPrice,
			l.MarketCap,
			l.TotalShares,
			l.Forecast,
			l.Demand,
			l.BenefitRequirement,
			l.BenefitDescription,
			l.calc_checksum,
			l.sys_inserted
		)
	WHEN NOT MATCHED BY SOURCE AND (s.sys_isdeleted=0)
	THEN UPDATE SET
		s.sys_isdeleted=1,
		s.sys_updated=SYSDATETIME()
	OUTPUT INSERTED.StockKey,
		$action,
		INSERTED.sys_isdeleted,
		COALESCE(INSERTED.sys_updated, INSERTED.sys_inserted)
	INTO @stage_details (
		[ElementKey],
		[MergeAction],
		[EntryIsDeleted],
		[DataLoadedTimestamp]
	);

	EXEC [memoir].[Log_Stage_Merge] 'Stocks', @stage_details, @CollectorLogKey;

	-- capture values used for dv procesisng
	DECLARE @change_details TABLE (
		dv_hashkey VARBINARY(20) NOT NULL,
		dv_gen_hashdiff VARBINARY(20) NOT NULL,
		dv_price_hashdiff VARBINARY(20) NOT NULL,
		dv_timestamp DATETIME2(7) NOT NULL,
		StockId INT NULL,
		StockAcronym NVARCHAR(100) NULL,
		StockKey BIGINT NOT NULL
	);

	-- create hashes for the dv hub key and satellite table data
	/* business keys in the data vault differ from the staging area in order to better represent the entities being tracked. the business key will be further abstracted in the kimball layer to make reporting easier. */
	INSERT INTO @change_details (dv_hashkey, dv_gen_hashdiff, dv_price_hashdiff, dv_timestamp, StockId, StockAcronym, StockKey)
	SELECT dbo.Create_Checksum(CONCAT(CAST(s.[StockId] AS NVARCHAR(MAX)), s.[StockAcronym])) AS dv_hashkey,
		dbo.Create_Checksum(CONCAT(CAST(s.[StockName] AS NVARCHAR(MAX)), s.[DirectorName], s.[BenefitDescription], s.[BenefitDescription], s.[sys_isdeleted])) AS dv_gen_hashdiff,
		dbo.Create_Checksum(CONCAT(CAST(s.[CurrentPrice] AS NVARCHAR(MAX)), s.[MarketCap], s.[TotalShares], s.[Forecast], s.[Demand])) AS dv_price_hashdiff,
		COALESCE(s.[sys_updated], s.[sys_inserted]) AS dv_timestamp,
		s.StockId,
		s.StockAcronym,
		s.StockKey
	FROM [stage].[Stocks] AS s;

	-- add new entries to hub
	INSERT INTO [dv].[hub_Stock] (
		[H_Stock_HK],
		[StockId_BK],
		[StockAcronym_BK],
		[H_Stock_LDTS]
	)
	SELECT chgd.[dv_hashkey],
		chgd.[StockId],
		chgd.[StockAcronym],
		chgd.[dv_timestamp]
	FROM @change_details AS chgd
	WHERE NOT EXISTS (
			SELECT *
			FROM [dv].[hub_Stock] AS dvh
			WHERE dvh.[H_Stock_HK]=chgd.[dv_hashkey]
		);

	EXEC [memoir].[Log_DataVault_Entry] 'hub_Stock', @@ROWCOUNT, @CollectorLogKey;

	-- update the general stock info satellite
	INSERT INTO [dv].[sat_Stock] (
		[H_Stock_HK],
		[StockName],
		[DirectorName],
		[BenefitRequirement],
		[BenefitDescription],
		[sys_isdeleted],
		[sys_stagekey],
		[S_Stock_HDIFF],
		[S_Stock_LDTS]
	)
	SELECT chgd.[dv_hashkey],
		s.[StockName],
		s.[DirectorName],
		s.[BenefitRequirement],
		s.[BenefitDescription],
		s.[sys_isdeleted],
		s.[StockKey],
		chgd.[dv_gen_hashdiff],
		chgd.[dv_timestamp]
	FROM @change_details AS chgd
	LEFT JOIN (
		SELECT [H_Stock_HK], [S_Stock_HDIFF] AS latest_Stock_HDIFF
		FROM (
			SELECT [H_Stock_HK], [S_Stock_HDIFF],
				ROW_NUMBER() OVER(PARTITION BY [H_Stock_HK] ORDER BY [S_Stock_LDTS] DESC) AS rnum
			FROM dv.sat_Stock
		) AS i
		WHERE rnum=1
	) AS sat_latest ON sat_latest.[H_Stock_HK] = chgd.[dv_hashkey]
	INNER JOIN [stage].[Stocks] AS s ON s.[StockKey] = chgd.[StockKey]
	WHERE sat_latest.[H_Stock_HK] IS NULL OR sat_latest.latest_Stock_HDIFF != chgd.[dv_gen_hashdiff];

	EXEC [memoir].[Log_DataVault_Entry] 'sat_Stock', @@ROWCOUNT, @CollectorLogKey;

	-- update the stock price info satellite
	INSERT INTO [dv].[sat_Stock_Price] (
		[H_Stock_HK],
		[CurrentPrice],
		[MarketCap],
		[TotalShares],
		[Forecast],
		[Demand],
		[S_Stock_Price_HDIFF],
		[S_Stock_Price_LDTS]
	)
	SELECT chgd.[dv_hashkey],
		s.[CurrentPrice],
		s.[MarketCap],
		s.[TotalShares],
		s.[Forecast],
		s.[Demand],
		chgd.[dv_price_hashdiff],
		chgd.[dv_timestamp]
	FROM @change_details AS chgd
	LEFT JOIN (
		SELECT [H_Stock_HK], [S_Stock_Price_HDIFF] AS latest_Stock_Price_HDIFF
		FROM (
			SELECT [H_Stock_HK], [S_Stock_Price_HDIFF],
				ROW_NUMBER() OVER(PARTITION BY [H_Stock_HK] ORDER BY [S_Stock_Price_LDTS] DESC) AS rnum
			FROM dv.sat_Stock_Price
		) AS i
		WHERE rnum=1
	) AS sat_latest ON sat_latest.[H_Stock_HK] = chgd.[dv_hashkey]
	INNER JOIN [stage].[Stocks] AS s ON s.[StockKey] = chgd.[StockKey]
	WHERE sat_latest.[H_Stock_HK] IS NULL OR sat_latest.latest_Stock_Price_HDIFF != chgd.[dv_price_hashdiff];

	EXEC [memoir].[Log_DataVault_Entry] 'sat_Stock_Price', @@ROWCOUNT, @CollectorLogKey;
END;
