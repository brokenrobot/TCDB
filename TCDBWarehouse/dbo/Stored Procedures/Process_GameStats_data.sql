﻿CREATE PROCEDURE [dbo].[Process_GameStats_data] (
	@CollectorLogKey BIGINT
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @stage_details [memoir].[StageMergeDetails];

	-- move landed data into the staging area
	MERGE INTO [stage].[GameStats] AS s
	USING [landing].[GameStats] AS l
	ON s.StatName=l.StatName
	WHEN MATCHED AND (l.StatValue!=s.StatValue OR s.sys_isdeleted=1)
	THEN UPDATE SET
		s.StatValue=l.StatValue,
		s.sys_isdeleted=0,
		s.sys_updated=l.sys_inserted
	WHEN NOT MATCHED BY TARGET
	THEN INSERT (
			StatName,
			StatValue,
			sys_inserted
		)
		VALUES (
			l.StatName,
			l.StatValue,
			l.sys_inserted
		)
	WHEN NOT MATCHED BY SOURCE AND (s.sys_isdeleted=0)
	THEN UPDATE SET
		s.sys_isdeleted=1,
		s.sys_updated=SYSDATETIME()
	OUTPUT INSERTED.GameStatKey,
		$action,
		INSERTED.sys_isdeleted,
		COALESCE(INSERTED.sys_updated, INSERTED.sys_inserted)
	INTO @stage_details (
		[ElementKey],
		[MergeAction],
		[EntryIsDeleted],
		[DataLoadedTimestamp]
	);

	EXEC [memoir].[Log_Stage_Merge] 'GameStats', @stage_details, @CollectorLogKey;

	-- capture values used for dv procesisng
	DECLARE @change_details TABLE (
		dv_hashkey VARBINARY(20) NOT NULL,
		dv_hashdiff VARBINARY(20) NOT NULL,
		dv_timestamp DATETIME2(7) NOT NULL,
		GameStatKey BIGINT NOT NULL,
		StatName NVARCHAR(50) NOT NULL
	);

	-- create the hashed business key
	/* this table is so simple that it is easier to just compare the 2 values than create a hashdiff column. if this table grows that should be reconsidered... */
	INSERT INTO @change_details (dv_hashkey, dv_hashdiff,  dv_timestamp, GameStatKey, StatName)
	SELECT dbo.Create_Checksum(s.[StatName]) AS dv_hashkey,
		dbo.Create_Checksum(CONCAT(CAST(s.StatValue AS NVARCHAR(MAX)), s.sys_isdeleted)) AS dv_hashdiff,
		COALESCE(s.[sys_updated], s.[sys_inserted]) AS dv_timestamp,
		s.[GameStatKey],
		s.[StatName]
	FROM [stage].[GameStats] AS s;

	-- insert new hub entries
	INSERT INTO [dv].[hub_GameStat] (
		[H_GameStat_HK],
		[StatName_BK],
		[H_GameStat_LDTS]
	)
	SELECT chgd.[dv_hashkey],
		chgd.StatName,
		chgd.dv_timestamp
	FROM @change_details AS chgd
	WHERE NOT EXISTS (
			SELECT *
			FROM [dv].[hub_GameStat] AS dvh
			WHERE dvh.[H_GameStat_HK] = chgd.[dv_hashkey]
		);

	EXEC [memoir].[Log_DataVault_Entry] 'hub_GameStat', @@ROWCOUNT, @CollectorLogKey;

	-- insert new data into satellite
	INSERT INTO [dv].[sat_GameStat] (
		[H_GameStat_HK],
		[StatValue],
		[sys_isdeleted],
		[sys_stagekey],
		[S_GameStat_HDIFF],
		[S_GameStat_LDTS]
	)
	SELECT chgd.[dv_hashkey],
		s.[StatValue],
		s.[sys_isdeleted],
		s.[GameStatKey],
		chgd.[dv_hashdiff],
		chgd.[dv_timestamp]
	FROM @change_details AS chgd
	LEFT JOIN (
		SELECT [H_GameStat_HK], [S_GameStat_HDIFF]
		FROM (
			SELECT [H_GameStat_HK], [S_GameStat_HDIFF],
				ROW_NUMBER() OVER(PARTITION BY [H_GameStat_HK] ORDER BY [S_GameStat_LDTS] DESC) AS rnum
			FROM [dv].[sat_GameStat]
		) as i
		WHERE rnum=1
	) AS sat_latest ON sat_latest.[H_GameStat_HK] = chgd.[dv_hashkey]
	INNER JOIN [stage].[GameStats] AS s ON s.[GameStatKey] = chgd.[GameStatKey]
	WHERE sat_latest.[H_GameStat_HK] IS NULL OR sat_latest.[S_GameStat_HDIFF] != chgd.[dv_hashdiff];

	EXEC [memoir].[Log_DataVault_Entry] 'sat_GameStat', @@ROWCOUNT, @CollectorLogKey;
END