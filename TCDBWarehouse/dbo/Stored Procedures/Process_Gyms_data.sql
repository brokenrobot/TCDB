﻿CREATE PROCEDURE [dbo].[Process_Gyms_data] (
	@CollectorLogKey BIGINT
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @stage_details [memoir].[StageMergeDetails];

	-- move landed data into the staging area
	MERGE INTO [stage].[Gyms] AS s
	USING [landing].[Gyms] AS l
	ON s.GymId=l.GymId
	WHEN MATCHED AND (l.calc_checksum!=s.sys_checksum OR s.sys_isdeleted=1)
	THEN UPDATE SET
		s.GymName=l.GymName,
		s.Stage=l.Stage,
		s.Cost=l.Cost,
		s.Energy=l.Energy,
		s.Strength=l.Strength,
		s.Speed=l.Speed,
		s.Defense=l.Defense,
		s.Dexterity=l.Dexterity,
		s.Note=l.Note,
		s.sys_isdeleted=0,
		s.sys_checksum=l.calc_checksum,
		s.sys_updated=l.sys_inserted
	WHEN NOT MATCHED BY TARGET
	THEN INSERT (
			GymId,
			GymName,
			Stage,
			Cost,
			Energy,
			Strength,
			Speed,
			Defense,
			Dexterity,
			Note,
			sys_checksum,
			sys_inserted
		)
		VALUES (
			l.GymId,
			l.GymName,
			l.Stage,
			l.Cost,
			l.Energy,
			l.Strength,
			l.Speed,
			l.Defense,
			l.Dexterity,
			l.Note,
			l.calc_checksum,
			l.sys_inserted
		)
	WHEN NOT MATCHED BY SOURCE AND (s.sys_isdeleted=0)
	THEN UPDATE SET
		s.sys_isdeleted=1,
		s.sys_updated=SYSDATETIME()
	OUTPUT INSERTED.GymKey,
		$action,
		INSERTED.sys_isdeleted,
		COALESCE(INSERTED.sys_updated, INSERTED.sys_inserted)
	INTO @stage_details (
		[ElementKey],
		[MergeAction],
		[EntryIsDeleted],
		[DataLoadedTimestamp]
	);

	EXEC [memoir].[Log_Stage_Merge] 'Gyms', @stage_details, @CollectorLogKey;

	-- capture values used for dv procesisng
	DECLARE @change_details TABLE (
		dv_hashkey VARBINARY(20) NOT NULL,
		dv_hashdiff VARBINARY(20) NOT NULL,
		dv_timestamp DATETIME2(7) NOT NULL,
		GymKey BIGINT NOT NULL,
		GymId INT NOT NULL,
		GymName NVARCHAR(50) NOT NULL
	);

	INSERT INTO @change_details (dv_hashkey, dv_hashdiff, dv_timestamp, GymKey, GymId, GymName)
	SELECT dbo.Create_Checksum(CONCAT(CAST(s.[GymId] AS NVARCHAR(MAX)), s.[GymName])) AS dv_hashkey,
		dbo.Create_Checksum(CONCAT(CAST(s.[Stage] AS NVARCHAR(MAX)), s.[Cost], s.[Energy], s.[Strength], s.[Speed], s.[Defense], s.[Dexterity], s.[Note], s.[sys_isdeleted])) as dv_hashdiff,
		COALESCE(s.[sys_updated], s.[sys_inserted]) AS dv_timestamp,
		s.[GymKey],
		s.[GymId],
		s.[GymName]
	FROM [stage].[Gyms] AS s;

	-- insert new hub entries
	INSERT INTO [dv].[hub_Gym] (
		[H_Gym_HK],
		[GymId_BK],
		[GymName_BK],
		[H_Gym_LDTS]
	)
	SELECT chgd.[dv_hashkey],
		chgd.GymId,
		chgd.GymName,
		chgd.dv_timestamp
	FROM @change_details AS chgd
	WHERE NOT EXISTS (
			SELECT *
			FROM [dv].[hub_Gym] AS dvh
			WHERE dvh.[H_Gym_HK] = chgd.[dv_hashkey]
		);

	EXEC [memoir].[Log_DataVault_Entry] 'hub_Gym', @@ROWCOUNT, @CollectorLogKey;

	-- insert new data into satellite
	INSERT INTO [dv].[sat_Gym] (
		[H_Gym_HK],
		[Stage],
		[Cost],
		[Energy],
		[Strength],
		[Speed],
		[Defense],
		[Dexterity],
		[Note],
		[sys_isdeleted],
		[sys_stagekey],
		[S_Gym_HDIFF],
		[S_Gym_LDTS]
	)
	SELECT chgd.[dv_hashkey],
		s.[Stage],
		s.[Cost],
		s.[Energy],
		s.[Strength],
		s.[Speed],
		s.[Defense],
		s.[Dexterity],
		s.[Note],
		s.[sys_isdeleted],
		s.[GymKey],
		chgd.[dv_hashdiff],
		chgd.[dv_timestamp]
	FROM @change_details AS chgd
	LEFT JOIN (
		SELECT [H_Gym_HK], [S_Gym_HDIFF]
		FROM (
			SELECT [H_Gym_HK], [S_Gym_HDIFF],
				ROW_NUMBER() OVER(PARTITION BY [H_Gym_HK] ORDER BY [S_Gym_LDTS] DESC) AS rnum
			FROM [dv].[sat_Gym]
		) as i
		WHERE rnum=1
	) AS sat_latest ON sat_latest.[H_Gym_HK] = chgd.[dv_hashkey]
	INNER JOIN [stage].[Gyms] AS s ON s.[GymKey] = chgd.[GymKey]
	WHERE sat_latest.[H_Gym_HK] IS NULL OR sat_latest.[S_Gym_HDIFF] != chgd.[dv_hashdiff];

	EXEC [memoir].[Log_DataVault_Entry] 'sat_Gym', @@ROWCOUNT, @CollectorLogKey;
END;