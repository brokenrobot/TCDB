﻿CREATE PROCEDURE [dbo].[Process_Medals_data] (
	@CollectorLogKey BIGINT
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @stage_details [memoir].[StageMergeDetails];

	-- move landed data into the staging area
	MERGE INTO [stage].[Medals] AS s
	USING [landing].[Medals] AS l
	ON s.MedalId=l.MedalId
	WHEN MATCHED AND (l.calc_checksum!=s.sys_checksum OR s.sys_isdeleted=1)
	THEN UPDATE SET
		s.MedalName=l.MedalName,
		s.MedalDescription=l.MedalDescription,
		s.MedalType=l.MedalType,
		s.Circulation=l.Circulation,
		s.Rarity=l.Rarity,
		s.sys_isdeleted=0,
		s.sys_checksum=l.calc_checksum,
		s.sys_updated=l.sys_inserted
	WHEN NOT MATCHED BY TARGET
	THEN INSERT (
			MedalId,
			MedalName,
			MedalDescription,
			MedalType,
			Circulation,
			Rarity,
			sys_checksum,
			sys_inserted
		)
		VALUES (
			l.MedalId,
			l.MedalName,
			l.MedalDescription,
			l.MedalType,
			l.Circulation,
			l.Rarity,
			l.calc_checksum,
			l.sys_inserted
		)
	WHEN NOT MATCHED BY SOURCE AND (s.sys_isdeleted=0)
	THEN UPDATE SET
		s.sys_isdeleted=1,
		s.sys_updated=SYSDATETIME()
	OUTPUT INSERTED.MedalKey,
		$action,
		INSERTED.sys_isdeleted,
		COALESCE(INSERTED.sys_updated, INSERTED.sys_inserted)
	INTO @stage_details (
		[ElementKey],
		[MergeAction],
		[EntryIsDeleted],
		[DataLoadedTimestamp]
	);

	EXEC [memoir].[Log_Stage_Merge] 'Medals', @stage_details, @CollectorLogKey;

	-- capture values used for dv procesisng
	DECLARE @change_details TABLE (
		dv_hashkey VARBINARY(20) NOT NULL,
		dv_hashdiff VARBINARY(20) NOT NULL,
		dv_timestamp DATETIME2(7) NOT NULL,
		MedalKey BIGINT NOT NULL,
		MedalId INT NOT NULL,
		MedalName NVARCHAR(50) NOT NULL
	);

	INSERT INTO @change_details (dv_hashkey, dv_hashdiff, dv_timestamp, MedalKey, MedalId, MedalName)
	SELECT dbo.Create_Checksum(CONCAT(CAST(s.[MedalId] AS NVARCHAR(MAX)), s.[MedalName])) AS dv_hashkey,
		dbo.Create_Checksum(CONCAT(CAST(s.[MedalDescription] AS NVARCHAR(MAX)), s.[MedalType], s.[Circulation], s.[Rarity], s.[sys_isdeleted])) as dv_hashdiff,
		COALESCE(s.[sys_updated], s.[sys_inserted]) AS dv_timestamp,
		s.[MedalKey],
		s.[MedalId],
		s.[MedalName]
	FROM [stage].[Medals] AS s;

	-- insert new hub entries
	INSERT INTO [dv].[hub_Medal] (
		[H_Medal_HK],
		[MedalId_BK],
		[MedalName_BK],
		[H_Medal_LDTS]
	)
	SELECT chgd.[dv_hashkey],
		chgd.MedalId,
		chgd.MedalName,
		chgd.dv_timestamp
	FROM @change_details AS chgd
	WHERE NOT EXISTS (
			SELECT *
			FROM [dv].[hub_Medal] AS dvh
			WHERE dvh.[H_Medal_HK] = chgd.[dv_hashkey]
		);

	EXEC [memoir].[Log_DataVault_Entry] 'hub_Medal', @@ROWCOUNT, @CollectorLogKey;

	-- insert new data into satellite
	INSERT INTO [dv].[sat_Medal] (
		[H_Medal_HK],
		[MedalDescription],
		[MedalType],
		[Circulation],
		[Rarity],
		[sys_isdeleted],
		[sys_stagekey],
		[S_Medal_HDIFF],
		[S_Medal_LDTS]
	)
	SELECT chgd.[dv_hashkey],
		s.[MedalDescription],
		s.[MedalType],
		s.[Circulation],
		s.[Rarity],
		s.[sys_isdeleted],
		s.[MedalKey],
		chgd.[dv_hashdiff],
		chgd.[dv_timestamp]
	FROM @change_details AS chgd
	LEFT JOIN (
		SELECT [H_Medal_HK], [S_Medal_HDIFF]
		FROM (
			SELECT [H_Medal_HK], [S_Medal_HDIFF],
				ROW_NUMBER() OVER(PARTITION BY [H_Medal_HK] ORDER BY [S_Medal_LDTS] DESC) AS rnum
			FROM [dv].[sat_Medal]
		) as i
		WHERE rnum=1
	) AS sat_latest ON sat_latest.[H_Medal_HK] = chgd.[dv_hashkey]
	INNER JOIN [stage].[Medals] AS s ON s.[MedalKey] = chgd.[MedalKey]
	WHERE sat_latest.[H_Medal_HK] IS NULL OR sat_latest.[S_Medal_HDIFF] != chgd.[dv_hashdiff];

	EXEC [memoir].[Log_DataVault_Entry] 'sat_Medal', @@ROWCOUNT, @CollectorLogKey;
END;