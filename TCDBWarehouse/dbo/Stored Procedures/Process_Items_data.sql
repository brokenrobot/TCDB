﻿CREATE PROCEDURE [dbo].[Process_Items_data] (
	@CollectorLogKey BIGINT
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @stage_details [memoir].[StageMergeDetails];

	-- merge data from the landing table to staging
	MERGE INTO [stage].[Items] AS s
	USING [landing].[Items] AS l
	ON l.[ItemId] = s.[ItemId] AND l.[ItemName] = s.[ItemName]
	WHEN MATCHED AND (l.[calc_checksum] != s.[sys_checksum] OR s.[sys_isdeleted] = 1)
	THEN UPDATE	SET
		s.ItemDescription=l.ItemDescription,
		s.ItemType=l.ItemType,
		s.WeaponType=l.WeaponType,
		s.BuyPrice=l.BuyPrice,
		s.SellPrice=l.SellPrice,
		s.MarketValue=l.MarketValue,
		s.Circulation=l.Circulation,
		s.Effect=l.Effect,
		s.Requirement=l.Requirement,
		s.ImageUrl=l.ImageUrl,
		s.sys_isdeleted=0,
		s.sys_checksum=l.calc_checksum,
		s.sys_updated=l.sys_inserted
	WHEN NOT MATCHED BY TARGET
	THEN INSERT (
			ItemId,
			ItemName,
			ItemDescription,
			ItemType,
			WeaponType,
			BuyPrice,
			SellPrice,
			MarketValue,
			Circulation,
			Effect,
			Requirement,
			ImageUrl,
			sys_checksum,
			sys_inserted
		)
		VALUES (
			l.ItemId,
			l.ItemName,
			l.ItemDescription,
			l.ItemType,
			l.WeaponType,
			l.BuyPrice,
			l.SellPrice,
			l.MarketValue,
			l.Circulation,
			l.Effect,
			l.Requirement,
			l.ImageUrl,
			l.calc_checksum,
			l.sys_inserted
		)
	WHEN NOT MATCHED BY SOURCE AND (s.sys_isdeleted=0)
	THEN UPDATE SET
		s.sys_isdeleted=1,
		s.sys_updated=SYSDATETIME()
	OUTPUT INSERTED.ItemKey,
		$action,
		INSERTED.sys_isdeleted,
		COALESCE(INSERTED.sys_updated, INSERTED.sys_inserted)
	INTO @stage_details (
		[ElementKey],
		[MergeAction],
		[EntryIsDeleted],
		[DataLoadedTimestamp]
	);

	EXEC [memoir].[Log_Stage_Merge] 'Items', @stage_details, @CollectorLogKey;

	-- capture values used for dv procesisng
	DECLARE @change_details TABLE (
		dv_hashkey VARBINARY(20) NOT NULL,
		dv_gen_hashdiff VARBINARY(20) NOT NULL,
		dv_price_hashdiff VARBINARY(20) NOT NULL,
		dv_timestamp DATETIME2(7) NOT NULL,
		ItemId INT NULL,
		ItemName NVARCHAR(100) NULL,
		ItemKey BIGINT NOT NULL
	);


	-- create hashes for the dv hub key and satellite table data
	/* business keys in the data vault differ from the staging area in order to better represent the entities being tracked. the business key will be further abstracted in the kimball layer to make reporting easier. */
	INSERT INTO @change_details (dv_hashkey, dv_gen_hashdiff, dv_price_hashdiff, dv_timestamp, ItemId, ItemName, ItemKey)
	SELECT dbo.Create_Checksum(CONCAT(CAST(s.[ItemId] AS NVARCHAR(MAX)), s.[ItemName])) AS dv_hashkey,
		dbo.Create_Checksum(CONCAT(CAST(s.[ItemDescription] AS NVARCHAR(MAX)), s.[ItemType], s.[WeaponType], s.[Effect], s.[Requirement], s.[ImageUrl], s.[sys_isdeleted])) AS dv_gen_hashdiff,
		dbo.Create_Checksum(CONCAT(CAST(s.[BuyPrice] AS NVARCHAR(MAX)), s.[SellPrice], s.[MarketValue], s.[Circulation])) AS dv_price_hashdiff,
		COALESCE(s.[sys_updated], s.[sys_inserted]) AS sys_timestamp,
		s.[ItemId],
		s.[ItemName],
		s.[ItemKey]
	FROM [stage].[Items] AS s;

	-- add new entries to hub
	INSERT INTO [dv].[hub_Item] (
		[H_Item_HK],
		[ItemId_BK],
		[ItemName_BK],
		[H_Item_LDTS]
	)
	SELECT chgd.[dv_hashkey],
		chgd.[ItemId],
		chgd.[ItemName],
		chgd.[dv_timestamp]
	FROM @change_details AS chgd
	WHERE NOT EXISTS (
			SELECT *
			FROM [dv].[hub_Item] AS dvh
			WHERE dvh.[H_Item_HK]=chgd.[dv_hashkey]
		);

	EXEC [memoir].[Log_DataVault_Entry] 'hub_Item', @@ROWCOUNT, @CollectorLogKey;

	-- update the general item info satellite
	INSERT INTO [dv].[sat_Item] (
		[H_Item_HK],
		[ItemDescription],
		[ItemType],
		[WeaponType],
		[Effect],
		[Requirement],
		[ImageUrl],
		[sys_isdeleted],
		[sys_stagekey],
		[S_Item_HDIFF],
		[S_Item_LDTS]
	)
	SELECT chgd.[dv_hashkey],
		s.[ItemDescription],
		s.[ItemType],
		s.[WeaponType],
		s.[Effect],
		s.[Requirement],
		s.[ImageUrl],
		s.[sys_isdeleted],
		s.[ItemKey],
		chgd.[dv_gen_hashdiff],
		chgd.[dv_timestamp]
	FROM @change_details AS chgd
	LEFT JOIN (
		SELECT [H_Item_HK], [S_Item_HDIFF] AS latest_Item_HDIFF
		FROM (
			SELECT [H_Item_HK], [S_Item_HDIFF],
				ROW_NUMBER() OVER(PARTITION BY [H_Item_HK] ORDER BY [S_Item_LDTS] DESC) AS rnum
			FROM dv.sat_Item
		) AS i
		WHERE rnum=1
	) AS sat_latest ON chgd.[dv_hashkey] = sat_latest.[H_Item_HK]
	INNER JOIN [stage].[Items] AS s ON s.[ItemKey] = chgd.[ItemKey]
	WHERE sat_latest.[H_Item_HK] IS NULL OR sat_latest.latest_Item_HDIFF != chgd.[dv_gen_hashdiff];

	EXEC [memoir].[Log_DataVault_Entry] 'sat_Item', @@ROWCOUNT, @CollectorLogKey;

	-- update the item price info satellite
	INSERT INTO [dv].[sat_Item_Price] (
		[H_Item_HK],
		[BuyPrice],
		[SellPrice],
		[MarketValue],
		[Circulation],
		[S_Item_Price_HDIFF],
		[S_Item_Price_LDTS]
	)
	SELECT chgd.[dv_hashkey],
		s.[BuyPrice],
		s.[SellPrice],
		s.[MarketValue],
		s.[Circulation],
		chgd.[dv_price_hashdiff],
		chgd.[dv_timestamp]
	FROM @change_details AS chgd
	LEFT JOIN (
		SELECT [H_Item_HK], [S_Item_Price_HDIFF] AS latest_Item_Price_HDIFF
		FROM (
			SELECT [H_Item_HK], [S_Item_Price_HDIFF],
				ROW_NUMBER() OVER(PARTITION BY [H_Item_HK] ORDER BY [S_Item_Price_LDTS] DESC) AS rnum
			FROM dv.sat_Item_Price
		) AS i
		WHERE rnum=1
	) AS sat_latest ON chgd.[dv_hashkey] = sat_latest.[H_Item_HK]
	INNER JOIN [stage].[Items] AS s ON s.[ItemKey] = chgd.[ItemKey]
	WHERE sat_latest.[H_Item_HK] IS NULL OR sat_latest.latest_Item_Price_HDIFF != chgd.[dv_price_hashdiff];

	EXEC [memoir].[Log_DataVault_Entry] 'sat_Item_Price', @@ROWCOUNT, @CollectorLogKey;
END;