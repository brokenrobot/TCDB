#!/bin/bash
. ./upload_templates.sh
aws cloudformation validate-template --template-url https://eondata.s3-ap-southeast-2.amazonaws.com/cloudformation/tcdb/eon-tcdb-stack.yaml
aws cloudformation validate-template --template-url https://eondata.s3-ap-southeast-2.amazonaws.com/cloudformation/tcdb/tcdb.yaml
aws cloudformation validate-template --template-url https://eondata.s3-ap-southeast-2.amazonaws.com/cloudformation/tcdb/tcdb-collector-stack.yaml
aws cloudformation validate-template --template-url https://eondata.s3-ap-southeast-2.amazonaws.com/cloudformation/tcdb/tcdb-rds-stack.yaml
