#!/bin/bash

ec2_asg_name=$1

# get the instance id of the bastion host
EC2INST_ID=$(aws autoscaling describe-auto-scaling-groups --auto-scaling-group-name "$1" | jq -r .AutoScalingGroups[0].Instances[0].InstanceId)

if [ -z "$EC2INST_ID" ]
then
	echo "the host is not running..."
else
	echo "terminating ec2 host with instance id $EC2INST_ID"
	aws ec2 terminate-instances --instance-ids "$EC2INST_ID"
fi
