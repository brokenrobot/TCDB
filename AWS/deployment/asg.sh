#!/bin/bash

ROLE_NAME=$1
ASG_NAME=$2
#ASG_NAME="torncollector-bastion-autoscale"
#ROLE_NAME="bastion"

CURR_CAP=$( aws autoscaling describe-auto-scaling-groups --auto-scaling-group-names "$ASG_NAME" | jq .AutoScalingGroups[0].DesiredCapacity )

if [ $CURR_CAP == 0 ]
then
        echo "The $ROLE_NAME host is NOT running."
else
        echo "The $ROLE_NAME host is running."
fi


if [ "$3" == "--start" ]
then
        if [ $CURR_CAP == 0 ]
        then
                echo "Starting the $ROLE_NAME host..."
                aws autoscaling set-desired-capacity --auto-scaling-group-name "$ASG_NAME" --desired-capacity 1
        else
                echo "Not starting the $ROLE_NAME host because it is already running."
        fi

fi

if [ "$3" == "--stop" ]
then
        if [ $CURR_CAP == 0 ]
        then
                echo "Not stopping the $ROLE_NAME host because it is not running."
        else
                echo "Stopping the $ROLE_NAME host..."
                aws autoscaling set-desired-capacity --auto-scaling-group-name "$ASG_NAME" --desired-capacity 0
        fi
fi

