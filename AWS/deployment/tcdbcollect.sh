#!/bin/bash

if [ "$1" = "--update" ]
then
	echo "updating tcdb collector s3 bucket"
	aws s3 sync --delete --exclude collector.json ~/vsproj/TCDB/TCDBCollector/bin/Publish/linux s3://tcdb/bin/collector
else
	. ./asg.sh "data collector" TcdbDataCollectorAutoScaling $1
fi
