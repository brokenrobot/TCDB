#!/bin/bash

asgname="TcdbWebHostAutoScaling"

aws autoscaling describe-auto-scaling-instances | jq ".AutoScalingInstances[] | select(.AutoScalingGroupName==\"$asgname\").InstanceId"


if [ "$1" = "--update" ]
then
	echo "updating tcdb web app s3 bucket"
	aws s3 sync --delete --exclude collector.json ~/vsproj/TCDB/TCDBWeb/bin/Publish/linux s3://tcdb/bin/webs
elif [ "$1" = "--kill" ]
then
	. ./kill-ec2-host.sh "$asgname"
else
	. ./asg.sh "web host" "$asgname" "$1"
fi
