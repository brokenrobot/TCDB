#!/bin/bash
. ./upload_templates.sh
aws cloudformation update-stack --stack-name TCDB --capabilities CAPABILITY_NAMED_IAM --template-url "https://eondata.s3-ap-southeast-2.amazonaws.com/cloudformation/tcdb/eon-tcdb-stack.yaml" --parameters ParameterKey=TornApiKey,UsePreviousValue=true
