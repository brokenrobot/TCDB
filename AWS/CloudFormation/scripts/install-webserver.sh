#!/usr/bin/env bash

AWS_REGION="${AWS::Region}"
SECRET_ID="{TcdbWebHostCertArchiveSecret}"

ARCHIVE_S3PATH="${TcdsS3BucketName}/data/webs/cert"

BASE_PATH="/root/cb"
ARCHIVE_NAME="certbot.tar.gz"
ENCRYPT_NAME="certbot.pgp"

NGINX_CFG_CERTBOT="/root/nginx_certbot.config"
NGINX_CFG_FINAL="/root/nginx_final.conf"

ReadArchivePass() {
    aws secretsmanager get-secret-value --secret-id "$SECRET_ID" --region "$AWS_REGION" | jq -r ".SecretString"
}

GetRandomPassword() {
    aws secretsmanager get-random-password --exclude-punctuation --no-include-space --require-each-included-type --password-length {TcdbCertbotArchivePasswordLength}
}

NewArchivePass() {
    SNAM=$(aws secretsmanager put-secret-value --secret-id "$SECRET_ID" --region "$AWS_REGION" --secret-string $(GetRandomPassword) | jq -r ".Name")
    echo "Updated secret value for $SNAM"
}

BackupCertbotFiles() {
    # generate a new archive password and store it in AWS Secrets Manager
    NewArchivePass
    # archive the lets encrypt certbot files
    tar zpcf "$BASE_PATH/$ARCHIVE_NAME" -C / etc/letsencrypt/
    # encrypt the archive with the key value stored in AWS Secrets Manager
    ReadArchivePass | gpg2 -c --batch --passphrase-fd 0 -o "$BASE_PATH/$ENCRYPT_NAME" "$BASE_PATH/$ARCHIVE_NAME"
    # delete the unprotected archive
    rm -f "$BASE_PATH/$ARCHIVE_NAME"
    # upload the encrypted archive to s3
    aws s3 mv "$BASE_PATH/$ENCRYPT_NAME" "s3://$S3_PATH/$ENCRYPT_NAME"
}

RestoreCertbotFiles() {
    # download the encrypted archive
    aws s3 cp "s3://$S3_PATH/$ENCRYPT_NAME" "$BASE_PATH/$ENCRYPT_NAME"
    # decrypt the archive using the password stored in AWS Secrets Manager
    ReadArchivePass | gpg2 -d --batch --passphrase-fd 0 -o "$BASE_PATH/$ARCHIVE_NAME" "$BASE_PATH/$ENCRYPT_NAME"
    # extract archived certbot files to their location in /etc/letsencrypt/
    tar zxf "$BASE_PATH/$ARCHIVE_NAME" -C /
    # remove archive files
    rm -f "$BASE_PATH/$ARCHIVE_NAME"
    rm -f "$BASE_PATH/$ENCRYPT_NAME"
}

# init the working directory
if [ -d "$BASE_PATH" ]
then
    rm -rf "$BASE_PATH/*"
else
    mkdir "$BASE_PATH"
    chown root:root "$BASE_PATH"
    chmod 0700 "$BASE_PATH"
fi

# check for a backup archive in S3
if [ -z "$(aws s3 ls s3://$ARCHIVE_S3PATH/$ENCRYPT_NAME)" ]
then
    # no backup archive exists.
    # the cert request process must be done in full.
    
else
    # backup archive exists.
    # the certbot files will be restored to avoid re-requesting a new certificate (and hitting the lets encrypt rate limit.)
fi