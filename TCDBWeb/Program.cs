using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace EonData.TornCity.Tcdb.Web {
    public class Program {
        private const string socketPath = "/run/tcdbweb/tcdbweb.socket";
        public static void Main(string[] args) {
            //CreateHostBuilder(args).Build().Run();
            var webHost = new WebHostBuilder()
                    .UseContentRoot(Directory.GetCurrentDirectory())
                    .UseKestrel((options) => options.UseSystemd()) //.ListenUnixSocket(socketPath))
                    .ConfigureLogging((logging) => {
                        logging.AddAWSProvider(new AWS.Logger.AWSLoggerConfig() {
                            LogGroup = "tcdbweb",
                            DisableLogGroupCreation = true,
                            LogStreamNamePrefix = "webapp",
                            LogStreamNameSuffix = Amazon.Util.EC2InstanceMetadata.InstanceId
                        });
                    })
                    .ConfigureServices((services) => {
                        services.AddRouting();
                    })
                    .UseStartup<Startup>();

            webHost.Build().Run();
        }
    }
}